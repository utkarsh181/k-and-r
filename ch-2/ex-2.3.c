/* ex-2.3.c -- Hexadecimal string to Integer */

#include <stdio.h>
#include <ctype.h>

/* Return an equivalent integer from string of hexadecimal digit S. */
int
htoi (const char *s)
{
  int i, n;

  if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X'))
    i = 2;
  else
    i = 0;

  n = 0;
  while (isxdigit (s[i]))
    {
      if (isdigit (s[i]))
	n = 16 * n + (s[i] - '0');
      else if (islower (s[i]))
	n = 16 * n + (s[i] - 'a' + 10);
      else
	n = 16 * n + (s[i] - 'A' + 10);

      ++i;
    }

  return n;
}

int
main (void)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  while ((nread = getline (&line, &len, stdin)) != -1)
    printf ("%d\n", htoi (line));

  return 0;
}
