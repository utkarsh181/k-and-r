#include <stdio.h>
#include <string.h>
#include <limits.h>

/* Return the first location in S1 where any character from S2 occurs. */
int
any (const char *s1, const char *s2)
{
  char hash[UCHAR_MAX + 1] = { 0 };
  int i;

  if (s1 == NULL)
    {
      if (s2 == NULL)
	return 0;
      else
	return -1;
    }

  /* Hashing by Parth Seethala:
     https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_2:Exercise_5 */
  i = 0;
  while (s2[i] != '\0')
    hash[(int) s2[i++]] = 1;

  i = 0;
  while (s1[i] != '\0' && hash[(int) s1[i]] == 0)
    i++;

  if (s1[i] == '\0')
    return -1;
  else
    return i;
}

/* Test driver by Richard Heathfield:
   https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_2:Exercise_5 */
int
main (void)
{
  char *leftstr[] = {
    "",
    "a",
    "antidisestablishmentarianism",
    "beautifications",
    "characteristically",
    "deterministically",
    "electroencephalography",
    "familiarisation",
    "gastrointestinal",
    "heterogeneousness",
    "incomprehensibility",
    "justifications",
    "knowledgeable",
    "lexicographically",
    "microarchitectures",
    "nondeterministically",
    "organizationally",
    "phenomenologically",
    "quantifications",
    "representationally",
    "straightforwardness",
    "telecommunications",
    "uncontrollability",
    "vulnerabilities",
    "wholeheartedly",
    "xylophonically",
    "youthfulness",
    "zoologically"
  };

  char *rightstr[] = {
    "",
    "a",
    "the",
    "quick",
    "brown",
    "dog",
    "jumps",
    "over",
    "lazy",
    "fox",
    "get",
    "rid",
    "of",
    "windows",
    "and",
    "install",
    "linux"
  };

  size_t numlefts = sizeof leftstr / sizeof leftstr[0];
  size_t numrights = sizeof rightstr / sizeof rightstr[0];
  size_t left = 0;
  size_t right = 0;

  int passed = 0;
  int failed = 0;

  int pos = -1;
  char *ptr = NULL;

  for (left = 0; left < numlefts; left++)
    {
      for (right = 0; right < numrights; right++)
	{
	  pos = any (leftstr[left], rightstr[right]);
	  ptr = strpbrk (leftstr[left], rightstr[right]);

	  if (-1 == pos)
	    {
	      if (ptr != NULL)
		{
		  printf ("Test %ld/%ld failed.\n", left, right);
		  ++failed;
		}
	      else
		{
		  printf ("Test %ld/%ld passed.\n", left, right);
		  ++passed;
		}
	    }
	  else
	    {
	      if (ptr == NULL)
		{
		  printf ("Test %ld/%ld failed.\n", left, right);
		  ++failed;
		}
	      else
		{
		  if (ptr - leftstr[left] == pos)
		    {
		      printf ("Test %ld/%ld passed.\n", left, right);
		      ++passed;
		    }
		  else
		    {
		      printf ("Test %ld/%ld failed.\n", left, right);
		      ++failed;
		    }
		}
	    }
	}
    }

  printf ("\n\n# test cases: %d\n# passed cases: %d\n# failed cases: %d\n",
	  passed + failed, passed, failed);

  return 0;
}
