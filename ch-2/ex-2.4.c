#include <stdio.h>
#include <string.h>

#define MAXLEN 40		/* Maximum length for test string. */

void
squeeze_char (char *s, int c)
{

  int i, j;

  for (i = j = 0; s[i] != '\0'; i++)
    if (s[i] != c)
      s[j++] = s[i];

  s[j] = '\0';
}

void
squeeze_string (char *s1, const char *s2)
{
  int i;

  for (i = 0; s2[i] != '\0'; i++)
    squeeze_char (s1, s2[i]);
}

int
main (void)
{
  char *leftstr[] = {
    "",
    "a",
    "antidisestablishmentarianism",
    "beautifications",
    "characteristically",
    "deterministically",
    "electroencephalography",
    "familiarisation",
    "gastrointestinal",
    "heterogeneousness",
    "incomprehensibility",
    "justifications",
    "knowledgeable",
    "lexicographically",
    "microarchitectures",
    "nondeterministically",
    "organizationally",
    "phenomenologically",
    "quantifications",
    "representationally",
    "straightforwardness",
    "telecommunications",
    "uncontrollability",
    "vulnerabilities",
    "wholeheartedly",
    "xylophonically",
    "youthfulness",
    "zoologically"
  };

  char *rightstr[] = {
    "",
    "a",
    "the",
    "quick",
    "brown",
    "dog",
    "jumps",
    "over",
    "lazy",
    "fox",
    "get",
    "rid",
    "of",
    "windows",
    "and",
    "install",
    "linux"
  };

  char buffer[MAXLEN];
  size_t numlefts = sizeof leftstr / sizeof leftstr[0];
  size_t numrights = sizeof rightstr / sizeof rightstr[0];
  size_t left = 0, right = 0;

  for (left = 0; left < numlefts; left++)
    {
      for (right = 0; right < numrights; right++)
	{
	  strcpy (buffer, leftstr[left]);
	  printf ("Original: %s %s\n", leftstr[left], rightstr[right]);
	  squeeze_string (buffer, rightstr[right]);
	  printf ("Modified: %s %s\n\n", buffer, rightstr[right]);
	}
    }

  return 0;
}
