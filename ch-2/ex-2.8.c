#include <stdio.h>

/* Get N bits from position P */
unsigned
getbits (unsigned x, int p, int n)
{
  return (x >> (p + 1 - n)) & ~(~0 << n);
}

/* Right rotate X by N bit positions. */
unsigned
rightrot (unsigned x, int n)
{
  unsigned tmp;

  for (int i = 0; i < n; i++)
    {
      tmp = getbits (x, 0, 1);
      tmp <<= sizeof (x) * 8 - 1;
      x = (x >> 1) | tmp;
    }

  return x;
}

int
main (void)
{
  printf ("%u\n", rightrot (1, 32));
  return 0;
}
