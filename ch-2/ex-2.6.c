#include <stdio.h>

/* Set N bits of X from position P to rightmost N bits of Y */
unsigned
setbits (unsigned x, int p, int n, unsigned y)
{
  unsigned mask = ~(~0 << n);

  /* NOTE 2021-12-27: The expression '(x & ~(mask << (p+1-n)))' sets N bits of X
     from position P to 0.  Then, expression '((y & mask) << (p+1-n))' move
     masked rignmost N bits to desired field to the right end. */
  return (x & ~(mask << (p + 1 - n))) | ((y & mask) << (p + 1 - n));
}

int
main (void)
{
  printf ("%u\n", setbits (14, 2, 3, 4));
  return 0;
}
