#include <stdio.h>
#include <limits.h>

/* Count 1 bits in X. */
int
bitcounts (unsigned x)
{
  int b = 0;

  while (x)
    {
      b++;
      x &= (x - 1);
    }

  return b;
}

int
main (void)
{
  printf ("%d\n", bitcounts (UINT_MAX));
}
