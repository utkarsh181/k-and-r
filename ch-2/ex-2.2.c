/* ex-2.2.c -- Get a line without '&&' and '||' operators. */

#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */

int
main (void)
{
  int i;
  char c, line[MAXLINE];

  for (i = 0; i < MAXLINE - 1; ++i)
    {
      c = getchar ();

      if (c == '\n')
	{
	  line[i] = c;
	  ++i;
	  break;
	}
      else if (c == EOF)
	break;
      else
	line[i] = c;
    }

  line[i] = '\0';

  printf ("%s", line);

  return 0;
}
