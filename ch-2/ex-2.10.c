#include <stdio.h>
#include <ctype.h>

/* Convert uppercase letters C to lower case. */
int
lower (int c)
{
  return isupper (c) ? c - 'A' + 'a' : c;
}

int
main (void)
{
  int i;
  char name[] = "THE C PROGRAMMING LANGUAGE";

  for (i = 0; name[i] != '\0'; i++)
    name[i] = lower (name[i]);

  printf ("%s\n", name);

  return 0;
}
