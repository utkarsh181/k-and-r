#include <stdio.h>
#include <limits.h>
#include <float.h>

int
main (void)
{
  printf ("Constant                  Value  Description\n\n");

  printf ("CHAR_MAX %22d  maximum value of char\n", CHAR_MAX);
  printf ("CHAR_MIN %22d  minimum value of char\n", CHAR_MIN);
  printf ("SCHAR_MAX %21d  maximum value of signed char\n", SCHAR_MAX);
  printf ("SCHAR_MIN %21d  minimum value of signed char\n", SCHAR_MIN);
  printf ("UCHAR_MAX %21u  maximum value of unsigned char\n\n", UCHAR_MAX);

  printf ("SHRT_MAX %22hd  maximum value of short\n", SHRT_MAX);
  printf ("SHRT_MIN %22hd  minimum value of short\n", SHRT_MIN);
  printf ("USHRT_MAX %21u  maximum value of unsigned short\n\n", USHRT_MAX);

  printf ("INT_MAX %23d  maximum value of int\n", INT_MAX);
  printf ("INT_MIN %23d  minimum value of int\n", INT_MIN);
  printf ("UINT_MAX %22u  maximum value of unsigned int\n\n", UINT_MAX);

  printf ("LONG_MAX %22ld  maximum value of long\n", LONG_MAX);
  printf ("LONG_MIN %22ld  minimum value of long\n", LONG_MIN);
  printf ("ULONG_MAX %21lu  maximum value of unsigned long\n\n", ULONG_MAX);

  printf ("FLT_MAX %23e  maximum value of float\n", FLT_MAX);
  printf ("FLT_MIN %23e  minimum value of float\n\n", FLT_MIN);

  printf ("DBL_MAX %23e  maximum value of double\n", DBL_MAX);
  printf ("DBL_MIN %23e  minimum value of double\n\n", DBL_MIN);

  printf ("LDBL_MAX %22Le  maximum value of long double\n", LDBL_MAX);
  printf ("LDBL_MIN %22Le  minimum value of long double\n", LDBL_MIN);


  return 0;
}
