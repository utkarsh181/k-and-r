/* detab.c -- Replaces tabs in input with blanks */

/* TODO 2022-01-06: Test options. */

#include <stdio.h>

/* Distance between tab stops (for display of tab characters), in columns. */
int tab_width = 8;

/* Numeric value of leftmost/first column */
int first_column = 0;

static struct option const long_options[] =
  {
    {"tab-width", required_argument, NULL, 't'},
    {"first-column", required_argument, NULL, 'c'},
    {NULL, 0, NULL, 0}
  };

int
next_tabstop (int column)
{
  return tab_width * (column / tab_width + 1);
}

int
main (void)
{
  int i, c, tmp, column;
  int option_index = 0;

  while ((c = getopt_long (argc, argv, "t:c:", long_options, &option_index)) != -1)
    {
      switch (c)
	{
	case 't':
	  tab_width = atoi (optarg);
	  break;
	case 'c':
	  first_column = atoi (optarg);
	  break;
	}
    }

  column = first_column;
  while ((c = getchar ()) != EOF)
    {
      if (c == '\n')
	{
	  column = first_column;
	  putchar (c);
	}
      else if (c == '\t')
	{
	  tmp = next_tabstop (column);

	  while (column < tmp)
	    {
	      ++column;
	      putchar (' ');
	    }
	}
      else
	{
	  ++column;
	  putchar (c);
	}
    }

  return 0;
}
