#include <stdio.h>

/* TODO 2022-01-08: Try this exercises after learning 'fseek'.  Also check out
   Coreutils manual by evaluating this from inside Emacs:

   (info "(coreutils) tail invocation")

   Particularly checkout -f option. */

static struct option const long_options[] =
{
  {"lines", required_argument, NULL, 'n'},
  {NULL, 0, NULL, 0}
};
