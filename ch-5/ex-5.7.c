#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLINES	5000		/* Maximum number of lines to be stored. */
#define MAXLEN		1000		/* Maximum length of line. */
#define MAXSTORE	10000		/* Storage of line allocation. */

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i, j;

  /* By Richard:
   * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_16.
   * Earlier I was just catching error inside 'main'. */
  for (i = 0, j = 0; (c = getchar ()) != EOF && c != '\n'; ++i)
    {
      /* Ignore updating character array if limit is reached. */
      if (i < lim - 1)
	{
	  s[j] = c;
	  ++j;
	}
    }
  if (c == '\n')
    {
      if (i < lim)
	{
	  s[j] = c;
	  ++j;
	}
      ++i;
    }
  s[j] = '\0';
  return i;
}

/* Read lines into storage LINESTORE, with each subsequent lines pointed by
   LINEPTR, where maximum number of lines is MAXLINES. */
int
readlines (char linestore[], char *lineptr[], int maxlines)
{
  int len, nlines;
  char line[MAXLEN];

  nlines = 0;
  while ((len = get_line (line, MAXLEN)) > 0)
    {
      if (nlines >= maxlines && strlen (linestore) + len > MAXSTORE)
	return -1;
      else
	{
	  strcpy (linestore, line);
	  lineptr[nlines++] = linestore;
	  linestore += len + 1;	/* Move to next free position. */
	}
    }
  return nlines;
}

/* Write NLINES number of lines pointed by LINEPTR. */
void
writelines (char *lineptr[], int nlines)
{
  while (nlines-- > 0)
    printf ("%s", *lineptr++);
}

void
swap (char *v[], int i, int j)
{
  char *tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}

/* Sort V with indexes from LEFT TO RIGHT in increasing order. */
void
quicksort (char *v[], int left, int right)
{
  int i, last;

  if (left >= right)		/* Do nothing if array contains. */
    return;

  swap (v, left, (left + right) / 2);
  last = left;
  for (i = left + 1; i <= right; i++)
    if (strcmp (v[i], v[left]) < 0)
      swap (v, ++last, i);

  swap (v, left, last);
  quicksort (v, left, last - 1);
  quicksort (v, last + 1, right);
}

int
main (void)
{
  char linestore[MAXSTORE];	/* Line storage. */
  char *lineptr[MAXLINES];	/* Pointers to text lines. */
  int nlines;			/* Number of input lines read. */

  if ((nlines = readlines (linestore, lineptr, MAXLINES)) > 0)
    {
      quicksort (lineptr, 0, nlines - 1);
      writelines (lineptr, nlines);
      return 0;
    }
  else
    {
      printf ("error (main): input to big to sort.\n");
      return 1;
    }
}
