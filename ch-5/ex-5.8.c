#include <stdio.h>

static char daytab[2][13] = {
  {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
  {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int
is_leap_year (int year)
{
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

/* Return day of year from YEAR, MONTH and DAY. */
int
day_of_year (int year, int month, int day)
{
  if (year < 1752)
    {
      printf ("error (day_of_year): invalid year %d\n", year);
      return 0;
    }
  else if (month < 1 || month > 12)
    {
      printf ("error (day_of_year): invalid month %d\n", month);
      return 0;
    }
  else if (day < 1 || day > 31)
    {
      printf ("error (day_of_year): invalid day %d\n", day);
      return 0;
    }

  int i, leap;

  leap = is_leap_year (year);
  for (i = 1; i < month; i++)
    day += daytab[leap][i];
  return day;
}

/* Set month PMONTH and day PDAY from YEAR and day of year YEARDAY. */
int
month_day (int year, int yearday, int *pmonth, int *pday)
{
  int i, leap;

  if (year < 1752)
    {
      printf ("error (month_day): invalid year %d\n", year);
      return 0;
    }

  leap = is_leap_year (year);
  if (yearday < 1 || (leap && yearday > 366) || (!leap && yearday > 365))
    {
      printf ("error (month_day): invalid day of year %d\n", yearday);
      return 0;
    }

  for (i = 1; yearday > daytab[leap][i]; i++)
    yearday -= daytab[leap][i];
  *pmonth = i;
  *pday = yearday;
  return 1;
}

int
main (void)
{
  int year, month, day, yearday;

  for (year = 1970; year <= 2000; ++year)
    {
      for (yearday = 1; yearday < 366; ++yearday)
	{
	  if (!month_day (year, yearday, &month, &day))
	    {
	      printf ("month_day failed: %d %d\n", year, yearday);
	    }
	  else if (day_of_year (year, month, day) != yearday)
	    {
	      printf ("bad result: %d %d\n", year, yearday);
	      printf ("month = %d, day = %d\n", month, day);
	    }
	}
    }
  return 0;
}
