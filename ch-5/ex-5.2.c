#include <stdio.h>
#include <ctype.h>

/* Get a floating-point number into PN. */
int
getint (double *pn)
{
  int c, sign;
  double power;

  while (isspace (c = getc (stdin))) /* Skip white spaces. */
    ;
  if (!isdigit (c) && c != EOF && c != '+' && c != '-' && c != '.')
    {
      ungetc (c, stdin);
      return 0;
    }
  sign = (c == '-') ? -1 : 1;
  if (c == '+' || c == '-')
    {
      c = getc (stdin);
      if (!isdigit (c))
	{
	  ungetc ((sign == 1) ? '+' : '-', stdin);
	  return 0;
	}
    }
  for (*pn = 0; isdigit (c); c = getc (stdin))
    *pn = 10.0 * *pn + (c - '0');
  if (c == '.')
    {
      c = getc (stdin);
      for (power = 1.0; isdigit (c); c = getc (stdin))
	{
	  *pn = 10.0 * *pn + (c - '0');
	  power *= 10.0;
	}
      *pn /= power;
    }
  *pn *= sign;
  if (c != EOF)
    ungetc (c, stdin);
  return c;
}

int
main (void)
{
  int x;
  double y;
  x = getint (&y);
  printf ("x = %d, y = %g\n", x, y);
  return 0;
}
