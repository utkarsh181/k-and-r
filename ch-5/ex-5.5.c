#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUF 16

char *
ut_strncpy (char *dest, const char *src, size_t n)
{
  size_t i;

  for (i = 0; i < n && src[i] != '\0'; i++)
    dest[i] = src[i];
  for (; i < n; i++)
    dest[i] = '\0';
  return dest;
}

char *
ut_strncat (char *dest, const char *src, size_t n)
{
  size_t i, j;

  for (i = 0; i < n && dest[i] != '\0'; i++)
    ;
  for (j = 0; i < n && src[j] != '\0'; i++, j++)
    dest[i] = src[j];
  return dest;
}

int
ut_strncmp (const char *s1, const char *s2, size_t n)
{
  size_t i;

  for (i = 0; i < n && s1[i] == s2[i]; i++)
    {
      if (s1[i] == '\0')
	return 0;
    }
  return s1[i] - s2[i];
}

/* Test cases by Lars Wirzenius:
   <https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_5:Exercise_5>. */

void
test_ncpy (const char *ut_str)
{
  char std_buf[MAX_BUF];
  char liw_buf[MAX_BUF];

  memset (std_buf, 0x42, sizeof (std_buf));
  ut_strncpy (std_buf, ut_str, sizeof (std_buf));

  memset (liw_buf, 0x42, sizeof (liw_buf));
  ut_strncpy (liw_buf, ut_str, sizeof (liw_buf));

  if (memcmp (std_buf, liw_buf, sizeof (std_buf)) != 0)
    {
      fprintf (stderr, "liw_ut_strncpy failed for <%s>\n", ut_str);
      exit (EXIT_FAILURE);
    }
}

void
test_ncat (const char *first, const char *second)
{
  char std_buf[MAX_BUF];
  char buf[MAX_BUF];

  memset (std_buf, 0x69, sizeof (std_buf));
  strcpy (std_buf, first);
  ut_strncat (std_buf, second, sizeof (std_buf) - strlen (std_buf) - 1);

  memset (buf, 0x69, sizeof (buf));
  strcpy (buf, first);
  ut_strncat (buf, second, sizeof (buf) - strlen (buf) - 1);

  if (memcmp (std_buf, buf, sizeof (std_buf)) != 0)
    {
      fprintf (stderr, "ut_strncat failed, <%s> and <%s>\n", first, second);
      exit (EXIT_FAILURE);
    }
}

void
test_ncmp (const char *first, const char *second)
{
  size_t len;
  int std_ret, ret;

  if (strlen (first) < strlen (second))
    len = strlen (second);
  else
    len = strlen (first);
  std_ret = ut_strncmp (first, second, len);
  ret = ut_strncmp (first, second, len);
  if ((std_ret < 0 && ret >= 0) || (std_ret > 0 && ret <= 0) ||
      (std_ret == 0 && ret != 0))
    {
      fprintf (stderr, "ut_strncmp failed, <%s> and <%s>\n", first, second);
      exit (EXIT_FAILURE);
    }
}

int
main (void)
{
  test_ncpy ("");
  test_ncpy ("a");
  test_ncpy ("ab");
  test_ncpy ("abcdefghijklmnopqrstuvwxyz");	/* longer than MAX_BUF */

  test_ncat ("", "a");
  test_ncat ("a", "bc");
  test_ncat ("ab", "cde");
  test_ncat ("ab", "cdefghijklmnopqrstuvwxyz");	/* longer than MAX_BUF */

  test_ncmp ("", "");
  test_ncmp ("", "a");
  test_ncmp ("a", "a");
  test_ncmp ("a", "ab");
  test_ncmp ("abc", "ab");

  printf ("All tests pass.\n");
  return 0;
}
