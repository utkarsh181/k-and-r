/* dcl.c --- C declaration to words and vice vera. */

/* TODO 2022-01-09: Complete exercise 5.20, which introduces dcl's with function
   argument types, const, etc. */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>
#include "getch.h"

#define MAXTOKEN	100	/* Maximum size of token. */
#define MAXBUF		1000	/* Maximum size of buffer. */

enum
{ NAME, PARENS, BRACKETS };	/* Types of tokens. */

int tokentype;			/* Type of last token. */
char token[MAXTOKEN];		/* Last token string. */
char name[MAXTOKEN];		/* Identifier name. */
char datatype[MAXTOKEN];	/* Data type = char, int, etc. */
char out[MAXBUF];		/* Ouput string. */

/* Get next token. */
int
gettoken (void)
{
  int c;
  char *p = token;

  while ((c = getch ()) == ' ' || c == '\t')
    ;
  if (c == '(')
    {
      if ((c = getch ()) == ')')
	{
	  strcpy (token, "()");
	  return tokentype = PARENS;
	}
      else
	{
	  ungetch (c);
	  return tokentype = '(';
	}
    }
  else if (c == '[')
    {
      for (*p++ = c; (*p++ = getch ()) != ']';)
	;
      *p = '\0';
      return tokentype = BRACKETS;
    }
  else if (isalpha (c))
    {
      for (*p++ = c; isalnum (c = getch ());)
	*p++ = c;
      *p = '\0';
      ungetch (c);
      return tokentype = NAME;
    }
  else
    return tokentype = c;
}

/* Parse a declarator. */
void
dcl (void)
{
  int ns;
  void direct_dcl (void);

  for (ns = 0; gettoken () == '*';)
    ns++;
  direct_dcl ();
  while (ns-- > 0)
    strcat (out, " pointer to");
}

/* Parse a direct declarator. */
void
direct_dcl (void)
{
  int type;

  if (tokentype == '(')		/* ( dcl ) */
    {
      dcl ();
      if (tokentype != ')')
	printf ("error (direct_dcl): missing )\n");
    }
  else if (tokentype == NAME)	/* Variable name. */
    strcpy (name, token);
  else
    printf ("error (direct_dcl): expected name of (dcl)\n");

  while ((type = gettoken ()) == PARENS || type == BRACKETS)
    {
      if (type == PARENS)
	strcat (out, " function returing");
      else
	{
	  strcat (out, " array");
	  strcat (out, token);
	  strcat (out, " of");
	}
    }
}

/* Convert declaration to words. */
void
dcl_to_words (void)
{
  while (gettoken () != EOF)
    {
      strcpy (datatype, token);	/* 1st token on line is the datatype. */
      out[0] = '\0';
      dcl ();			/* Parse rest of line. */
      if (tokentype != '\n')
	printf ("error (dcl_to_words): syntax error\n");
      printf ("%s: %s %s\n", name, out, datatype);
    }
}

/* By codybartfast: <https://github.com/codybartfast/knr>.  I was just so
   confused by these "complicated" declaration, that I wasn't able to recognise
   patterns for extra parentheses. */
int
need_parens (void)
{
  int c1, c2, flag;

  flag = 0;
  if ((c1 = getch ()) == ' ')
    {
      if ((c2 = getch ()) == '(' || c2 == '[')
	flag = 1;
      ungetch (c2);
    }
  ungetch (c1);
  return flag;
}

/* Convert word description to declaration. */
void
words_to_dcl (void)
{
  int type;
  char tmp[MAXTOKEN];

  while (gettoken () != EOF)
    {
      strcpy (out, token);
      while ((type = gettoken ()) != '\n')
	{
	  if (type == PARENS || type == BRACKETS)
	    strcat (out, token);
	  else if (type == '*')
	    {
	      if (need_parens ())
		sprintf (tmp, "(*%s)", out);
	      else
		sprintf (tmp, "*%s", out);
	      strcpy (out, tmp);
	    }
	  else if (type == NAME)
	    {
	      sprintf (tmp, "%s %s", token, out);
	      strcpy (out, tmp);
	    }
	  else
	    printf ("error (words_to_dcl): invalid input at %s\n", token);
	}
      printf ("%s\n", out);
    }
}

int
main (int argc, char *argv[])
{
  struct option const long_options[] = {
    {"dcl-to-words", no_argument, NULL, 'd'},
    {"words-to-dcl", no_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}
  };
  char c;
  int option_index = 0;		/* 'getopt_long' store the option index here. */

  while ((c =
	  getopt_long (argc, argv, "du", long_options, &option_index)) > 0)
    {
      switch (c)
	{
	case 'd':
	  dcl_to_words ();
	  break;
	case 'u':
	  words_to_dcl ();
	  break;
	}
    }

  return 0;
}
