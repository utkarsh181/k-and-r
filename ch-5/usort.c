/* usort.c --- Sort lines of input. */

/* TODO 2022-01-09: Complete exercise 5.17, which introduces field-handling
   capabilities. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <getopt.h>

#define MAXLINE	5000		/* Maximum number of lines to be sorted */

static int reverse_flag = 0;	 /* Flag set by '--reverse'. */
static int ignore_case_flag = 0; /* Flag set by '--ignore-case'. */

enum { LEX_SORT, NUMERIC_SORT, DICTIONARY_SORT }; /* Types of sorting. */

/* Readlines lines into LINEPTR of maximum length MAXLINE from STREAM. */
ssize_t
readlines (FILE * stream, char *lineptr[], size_t maxline)
{
  size_t len, nlines;
  ssize_t nread;

  len = nlines = 0;
  while (nlines < maxline
	 && (nread = getline (&lineptr[nlines++], &len, stream)) != -1)
    ;
  return nlines;
}

/* Write NLINES lines from LINEPTR into STREAM. */
void
writelines (FILE * stream, char *lineptr[], ssize_t nlines)
{
  ssize_t i;

  if (reverse_flag)
    {
      for (i = nlines - 1; i >= 0; i--)
	fputs (lineptr[i], stream);
    }
  else
    {
      for (i = 0; i < nlines; i++)
	fputs (lineptr[i], stream);
    }
}

void
swap (void *v[], int i, int j)
{
  void *tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}

/* Compare S1 and S2 numerically. */
int
numcmp (char *s1, char *s2)
{
  double v1, v2;

  v1 = atof (s1);
  v2 = atof (s2);
  if (v1 < v2)
    return -1;
  else if (v1 > v2)
    return 1;
  else
    return 0;
}

/* TODO 2022-01-08: Test it. */
/* Compare S1 and S2 lexicographically, but only considering letters, numbers
   and blanks. */
int
dictionarycmp (char *s1, char *s2)
{
  size_t i;

  for (i = 0; s1[i] == s2[i]
       || !isblank (s1[i]) || !isblank (s2[i])
       || !isalnum (s1[i]) || !isalnum (s1[i]); i++)
    {
      if (s1[i] == '\0')
	return 0;
    }
  return s1[i] - s2[i];
}

/* Convert S to uppercase. */
char *
strtoupper (char *s)
{
  char *copy;

  copy = strdup (s);
  while (*s != '\0')
    *copy++ = *s++;

  return copy;
}

/* Sort V with from LEFT to RIGHT using COMP for comparison. */
void
quicksort (void *v[], int left, int right, int (*comp) (void *, void *))
{
  int i, last;

  /* Do nothing if array contains fewer than two elements. */
  if (left >= right)
    return;

  swap (v, left, (left + right) / 2);
  last = left;

  for (i = left + 1; i <= right; i++)
    {
      /* TODO 2022-01-08: Rather than using global variables, use structures to
         store information about V! */
      if (ignore_case_flag)
	{
	  char *vicpy = strtoupper (v[i]);
	  char *vleftcpy = strtoupper (v[left]);

	  if ((*comp) (vicpy, vleftcpy) < 0)
	    swap (v, ++last, i);

	  free (vicpy);
	  free (vleftcpy);
	}
      else
	{
	  if ((*comp) (v[i], v[left]) < 0)
	    swap (v, ++last, i);
	}
    }

  swap (v, left, last);
  quicksort (v, left, last - 1, comp);
  quicksort (v, last + 1, right, comp);
}

int
main (int argc, char *argv[])
{
  size_t nlines;
  char *lineptr[MAXLINE] = { NULL };	/* Pointers to text lines. */

  static struct option const long_options[] = {
    {"reverse", no_argument, NULL, 'r'},
    {"ignore-case", no_argument, NULL, 'f'},
    {"numeric-sort", no_argument, NULL, 'n'},
    {"dictionary-sort", no_argument, NULL, 'd'},
    {NULL, 0, NULL, 0}
  };
  char c;
  int option_index = 0;		/* 'getopt_long' store the option index here. */
  int sort_type;

  sort_type = LEX_SORT;		/* By default, do lexical sorting. */
  while ((c =
	  getopt_long (argc, argv, "rfnd", long_options, &option_index)) > 0)
    {
      switch (c)
	{
	case 'r':
	  reverse_flag = 1;
	  break;
	case 'f':
	  ignore_case_flag = 1;
	  break;
	case 'n':
	  sort_type = NUMERIC_SORT;
	  break;
	case 'd':
	  sort_type = DICTIONARY_SORT;
	  break;
	}
    }

  nlines = readlines (stdin, lineptr, MAXLINE);
  switch (sort_type)
    {
    case LEX_SORT:
      quicksort ((void **) lineptr, 0, nlines - 1,
		 (int (*)(void *, void *)) strcmp);
      break;
    case NUMERIC_SORT:
      quicksort ((void **) lineptr, 0, nlines - 1,
		 (int (*)(void *, void *)) numcmp);
      break;
    case DICTIONARY_SORT:
      quicksort ((void **) lineptr, 0, nlines - 1,
		 (int (*)(void *, void *)) dictionarycmp);
    }
  writelines (stdout, lineptr, nlines);
  return 0;
}
