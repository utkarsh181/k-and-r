#ifndef STACK_H
#define STACK_H	1

void push (double);		/* Push value to stack. */
double pop (void);		/* Pop value from stack. */
void print_tos (void);		/* Print Top Of Stack. */
void duplicate_tos (void);	/* Duplicate Top Of Stack. */
void swap_tos (void);		/* Swap Top Of Stack. */
void clear_stack (void);	/* Clear stack. */

#endif
