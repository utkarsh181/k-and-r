#include <stdio.h>

/* By Nicholas Smillie:
   <https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_5:Exercise_4>.  Earlier I
   was iteratively going to index 'strlen (s) - strlen (t)'. */
void
strend (char *s, char *t)
{
  *s += strlen (s) - strlen (t);
  while (*s++ == *t++)
    {
      if (*s == '\0')
	return 1;
    }
  return 0;
}

int main(void)
{
  char *s = "hello, hello";
  char *t = "llo";

  if (strend(s, t))
    printf("'%s' occurs at the end of '%s'\n", t, s);
  else
    printf("No occurences at the end of '%s' from '%s'\n", s, t);
  return 0;
}
