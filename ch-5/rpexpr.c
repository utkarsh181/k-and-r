/* rpexpr.c -- Evaluator for reverse Polish notation.

  Copyright (C) 2022 Utkarsh Singh

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>. */

/* Commentary:

   This program implements a evaluator for reverse Polish notation, in which
   each operator follows its operands; an expression like

   (1 - 2) * (4 + 5)

   is entered as

   1 2 - 4 5 + *

   Parentheses are not needed; the notation is unambiguous as long as know how
   many operands each operator expects.*/

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "stack.h"

#define MAXOP	100		/* Maximum size of operand or operator. */

#define NUMBER	'0'		/* Signal that a number was found. */
#define IDENTIFIER '1'		/* Signal that an identifier (ie. a math.h
				   function) was found. */

/* Get next operator or numeric operand from expression EXPR into OP. */
int
getop (char *expr, char *op)
{
  int i, c;

  /* Parse exprerator. */
  while ((op[0] = c = *expr++) == ' ' || c == '\t')
    ;
  if (c == '-' || c == '+')
    {
      op[1] = *expr++;
      if (!isdigit (op[1]))
	{
	  expr--;
	  op[1] = '\0';
	  return c;
	}
    }
  else if (!isalnum (c) && c != '.')
    {
      op[1] = '\0';
      return c;
    }

  i = 0;
  if (c == '-' || c == '+')
    c = op[++i];

  /* Parse identifier */
  if (isalpha (c))
    {
      while (isalpha (op[++i] = c = *expr++))
	;
      op[i] = '\0';
      if (c != EOF)
	expr--;
      return IDENTIFIER;
    }

  /* Parse number */
  if (isdigit (c))
    while (isdigit (op[++i] = c = *expr++))
      ;
  if (c == '.')
    while (isdigit (op[++i] = c = *expr++))
      ;
  op[i] = '\0';
  if (c != EOF)
    expr--;
  return NUMBER;
}

/* Evaluate IDENTIFIER. */
void
eval_identifier (char *identifier)
{
  double op2;

  if (strcmp (identifier, "sin") == 0)
    push (sin (pop ()));
  else if (strcmp (identifier, "cos") == 0)
    push (cos (pop ()));
  else if (strcmp (identifier, "tan") == 0)
    push (tan (pop ()));
  else if (strcmp (identifier, "exp") == 0)
    push (exp (pop ()));
  else if (strcmp (identifier, "pow") == 0)
    {
      op2 = pop ();
      push (pow (pop (), op2));
    }
  else
    printf ("error (eval_identifier): unknow identifier %s\n", identifier);
}

double
eval (char *exprs[], int len)
{
  int i, type;
  char op[MAXOP];
  double op2;

  for (i = 0; i < len; i++)
    {
      type = getop(exprs[i], op);
      switch (type)
	{
	case NUMBER:
	  push (atof (op));
	  break;
	case IDENTIFIER:
	  eval_identifier (op);
	  break;
	case '+':
	  push (pop () + pop ());
	  break;
	case '*':
	  push (pop () * pop ());
	  break;
	case '-':
	  op2 = pop ();
	  push (pop () - op2);
	  break;
	case '/':
	  op2 = pop ();
	  if (op2 != 0.0)
	    push (pop () / op2);
	  else
	    printf ("error (main): zero divisor\n");
	  break;
	case '%':
	  op2 = pop ();
	  if (op2 != 0.0)
	    push ((int) pop () % (int) op2);
	  else
	    printf ("error (main): zero divisor\n");
	  break;
	case '?':
	  print_tos ();
	  break;
	case '!':
	  clear_stack ();
	  break;
	case '#':
	  duplicate_tos ();
	  break;
	case '~':
	  swap_tos ();
	  break;
	default:
	  printf ("error (main): unknown command %s\n", op);
	  break;
	}
    }
  return pop();
}

int
main (int argc, char *argv[])
{
  if (argc < 2)
    {
      printf ("Usage: rpexpr <expression...> \n");
      return 1;
    }

  printf ("%.8g\n", eval (&argv[1], argc - 1));
  return 0;
}
