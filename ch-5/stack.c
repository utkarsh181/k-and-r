#include <stdio.h>

#define MAXVAL	100		/* Max depth of val stack. */

static int sp = 0;		/* Next free stack position */
static double val[MAXVAL];	/* Value stack. */

/* Push F onto value stack. */
void
push (double f)
{
  if (sp < MAXVAL)
    val[sp++] = f;
  else
    printf ("error (push): stack full, can't push %g\n", f);
}

/* Pop and return top value from stack. */
double
pop (void)
{
  if (sp > 0)
    return val[--sp];
  else
    {
      printf ("error (pop): stack empty\n");
      return 0.0;
    }
}

/* Print element at the Top Of the Stack (TOS) */
void
print_tos (void)
{
  if (sp < 0)
    printf ("Top of the stack is empty!\n");
  else
    putchar (val[sp - 1]);
}

/* Duplicate element at the Top Of the Stack (TOS) */
void
duplicate_tos (void)
{
  push (val[sp - 1]);
}

/* Swap element at the Top Of the Stack (TOS) */
void
swap_tos (void)
{
  int tmp = val[sp - 1];
  val[sp - 1] = val[sp - 2];
  val[sp - 2] = tmp;
}

void
clear_stack (void)
{
  sp = 0;
}
