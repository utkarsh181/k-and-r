#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

void
filecopy (int ifd, int ofd)
{
  int n;
  char buf[BUFSIZ];

  while ((n = read (ifd, buf, BUFSIZ)) > 0)
    write (ofd, buf, n);
}

int
main (int argc, char *argv[])
{
  int fd;
  char *prog = argv[0];

  if (argc == 1)		/* no args; copy standard input */
    filecopy (STDIN_FILENO, STDOUT_FILENO);
  else
    while (--argc > 0)
      if ((fd = open (*++argv, O_RDONLY, 0)) == -1)
	{
	  perror (prog);
	  return 1;
	}
      else
	{
	  filecopy (fd, STDOUT_FILENO);
	  close (fd);
	}
  return 0;
}
