#include <stddef.h>
#include <string.h>
#include <unistd.h>

#include "alloc.h"

typedef long Align;		/* For alignment to long boundary. */

union header
{				/* Block Header: */
  struct
  {
    union header *ptr;		/* Next block if on free list. */
    size_t size;		/* Size of this block. */
  } s;
  Align x;			/* Force alignment of blocks. */
};

typedef union header Header;
Header base;			/* Empty list to get started. */
Header *freep = NULL;		/* Start of free list. */

/* Put block AP in free list. */
void
free (void *ap)
{
  Header *bp, *p;

  bp = (Header *) ap - 1;	/* Point to block header. */
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;			/* Freed block at start or end of arena. */

  if (bp + bp->s.size == p->s.ptr)
    {				/* Join to upper nbr. */
      bp->s.size += p->s.ptr->s.size;
      bp->s.ptr = p->s.ptr->s.ptr;
    }
  else
    bp->s.ptr = p->s.ptr;
  if (p + p->s.size == bp)
    {				/* Join to lower nbr. */
      p->s.size += bp->s.size;
      p->s.ptr = bp->s.ptr;
    }
  else
    p->s.ptr = bp;
  freep = p;
}

/* Ask system for more memory units NU. */
Header *
morecore (size_t nu)
{
  char *cp;
  Header *up;

  if (nu < NALLOC)
    nu = NALLOC;
  cp = sbrk (nu * sizeof (Header));
  if (cp == (char *) -1)	/* No space at all. */
    return NULL;
  up = (Header *) cp;
  up->s.size = nu;
  free ((void *) (up + 1));
  return freep;
}

/* Returns a pointer to a newly allocated block SIZE bytes long */
void *
malloc (size_t size)
{
  Header *p, *prevp;
  size_t nunits;

  nunits = (size + sizeof (Header) - 1) / sizeof (Header) + 1;
  if ((prevp = freep) == NULL)		/* No free list yet. */
    {
      base.s.ptr = freep = prevp = &base;
      base.s.size = 0;
    }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr)
    {
      if (p->s.size >= nunits)		/* Big enough. */
	{
	  if (p->s.size == nunits)	/* Exactly. */
	    prevp->s.ptr = p->s.ptr;
	  else				/* Allocate tail end. */
	    {
	      p->s.size -= nunits;
	      p += p->s.size;
	      p->s.size = nunits;
	    }
	  freep = prevp;
	  return (void *) (p + 1);
	}
      if (p == freep)			/* Wrapped around free list. */
	if ((p = morecore (nunits)) == NULL)
	  return NULL;			/* None left. */
    }
}

/* Allocate a block long enough to contain a vector of N elements, each of size
   SIZE. */
void *
calloc (size_t n, size_t size)
{
  void *value = malloc (n * size);
  if (value != 0)
    memset (value, 0, n * size);
  return value;
}

/* Free block P on size N. */
void
bfree (void *p, size_t n)
{
  size_t nunits = n / sizeof (Header);

  if (nunits > 1)
    {
      Header *bp = (Header *) p;
      bp->s.size = nunits;
      free ((void *) (bp + 1));
    }
}
