#ifndef ALLOC_H
#define ALLOC_H		1

#define    NALLOC    1024	/* Minimum #units to request. */

void free (void *);
void *malloc (size_t);
void *calloc (size_t, size_t);
void bfree (void *, size_t);

#endif
