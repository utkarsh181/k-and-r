#include <stdio.h>
#include "alloc.h"

int
main (void)
{
  int *p = NULL;
  int i = 0;

  p = calloc (100, sizeof *p);
  if (NULL == p)
    fprintf (stderr, "error: calloc returned NULL.\n");
  else
    {
      for (i = 0; i < 100; i++)
	{
	  printf ("%08x ", p[i]);
	  if (i % 8 == 7)
	      printf ("\n");
	}
      printf ("\n");
      free (p);
    }
  return 0;
}
