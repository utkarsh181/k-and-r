/* io_fields.c -- Implementation of standard library I/O functions where
   "fields" signifies use of C's bit fields. */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "io_fields.h"

#define MASQ	-2		/* A masquerading character. */

FILE _iob[OPEN_MAX] = {		/* Stdin, Stdout, Stderr: */
  {0, (char *) 0, (char *) 0, 1, 0, 0, 0, 0, 0},
  {0, (char *) 0, (char *) 0, 0, 1, 0, 0, 0, 1},
  {0, (char *) 0, (char *) 0, 0, 1, 1, 0, 0, 2}
};

/* Allocate and fill input buffer. */
int
_fillbuf (FILE * fp)
{
  int bufsize;

  if (!fp->is_read && fp->is_eof && fp->is_err)
    return EOF;

  bufsize = (fp->is_unbuf) ? 1 : BUFSIZ;
  if (fp->base == NULL)		/* No buffer yet. */
    if ((fp->base = (char *) malloc (bufsize)) == NULL)
      return EOF;		/* Can't get buffer. */

  fp->ptr = fp->base;
  fp->cnt = read (fp->fd, fp->ptr, bufsize);

  if (--fp->cnt < 0)
    {
      if (fp->cnt == -1)
	fp->is_eof = 1;
      else
	fp->is_err = 1;
      fp->cnt = 0;
      return EOF;
    }
  return (unsigned char) *fp->ptr++;
}

/* Allocate and flush input buffer. */
int
_flushbuf (int c, FILE * fp)
{
  int bufsize, nbytes;

  if (!fp->is_write && fp->is_eof && fp->is_err)
    return EOF;

  bufsize = (fp->is_unbuf) ? 1 : BUFSIZ;
  if (fp->base == NULL)		/* No buffer yet. */
    {
      if ((fp->base = (char *) malloc (bufsize)) == NULL)
	return EOF;		/* Can't get buffer. */
      else
	{
	  fp->ptr = fp->base;
	  fp->cnt = bufsize;
	}
    }

  if (c == EOF)
    fp->is_eof = 1;
  else if (c != MASQ)
    {
      *fp->ptr++ = c;
      fp->cnt--;
    }

  nbytes = fp->ptr - fp->base;
  if (write (fp->fd, fp->base, nbytes) != nbytes)
    {
      fp->is_err = 1;
      return EOF;
    }

  fp->ptr = fp->base;
  fp->cnt = bufsize;
  return c;
}

/* Open file, return file ptr. */
FILE *
fopen (char *name, char *mode)
{
  int fd;
  FILE *fp;

  if (*mode != 'r' && *mode != 'w' && *mode != 'a')
    return NULL;
  for (fp = _iob; fp < _iob + OPEN_MAX; fp++)
    if (!fp->is_read && !fp->is_write)
      break;			/* Found free slot. */
  if (fp >= _iob + OPEN_MAX)	/* No free slots. */
    return NULL;

  if (*mode == 'w')
    fd = creat (name, PERMS);
  else if (*mode == 'a')
    {
      if ((fd = open (name, O_WRONLY, 0)) == -1)
	fd = creat (name, PERMS);
      lseek (fd, 0L, 2);
    }
  else
    fd = open (name, O_RDONLY, 0);
  if (fd == -1)			/* Couldn't access name. */
    return NULL;
  fp->fd = fd;
  fp->cnt = 0;
  fp->base = NULL;
  if (*mode == 'r')
    fp->is_read = 1;
  else
    fp->is_write = 1;
  return fp;
}

int
fflush (FILE * fp)
{
  int result = EOF;

  if (fp == NULL)
    for (fp = _iob; fp < _iob + OPEN_MAX; fp++)
      {
	if (fp->is_write)
	  result = fflush (fp);
      }
  else if (fp->is_write)
    result = _flushbuf (MASQ, fp);
  else if (fp->is_read)		/* TODO 2022-07-28: Test it. */
    {
      fp->ptr = fp->base;
      fp->cnt = 0;
      result = 0;
    }
  return result != EOF ? 0 : EOF;
}

int
fclose (FILE * fp)
{
  if (fp == NULL)
    return EOF;

  fflush (fp);
  free (fp->base);
  fp->base = NULL;
  fp->ptr = NULL;
  if (fp->fd <= 2)
    return 0;
  fp->is_read = 0;
  fp->is_write = 0;
  return close (fp->fd);
}

/* Seek to a certain position on FP. */
int
fseek (FILE * fp, long offset, int whence)
{
  if (!fp->is_unbuf && fp->base != NULL)
    {
      /* By: Gregory Pietsch
       * <https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_8:Exercise_4#Second_solution:>.
       * Fix offset so that it's from the last character the user read instead
       * of the last character that "actually" read. */
      if (fp->is_read && whence == SEEK_CUR)
	{
	  if (offset < 0 || offset > fp->cnt)
	    offset -= fp->cnt;
	  else
	    {
	      fp->cnt -= offset;
	      fp->ptr += offset;
	      fp->is_eof = 0;
	      return 0;
	    }
	}
      if (fflush (fp) == -1)
	return EOF;
    }
  if (lseek (fp->fd, offset, whence) == -1)
    {
      fp->is_err = 1;
      return EOF;
    }
  fp->is_eof = 0;
  return 0;
}
