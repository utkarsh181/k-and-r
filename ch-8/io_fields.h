#ifndef IO_FIELDS_H
#define IO_FIELDS_H	1

#define EOF		(-1)
#define BUFSIZ		1024
#define OPEN_MAX	20	/* Max #files open at once. */
#define PERMS		0666	/* RW for owner, group, others. */

typedef struct _iobuf
{
  int cnt;			/* Characters left */
  char *ptr;			/* Next character position. */
  char *base;			/* Location of buffer. */
  unsigned int is_read:1;	/* File open for reading. */
  unsigned int is_write:1;	/* File open for writing. */
  unsigned int is_unbuf:1;	/* File is unbuffered */
  unsigned int is_eof:1;	/* EOF has occurred on this file. */
  unsigned int is_err:1;	/* Error occured on this file. */
  int fd;			/* File descriptor */
} FILE;

extern FILE _iob[OPEN_MAX];

#define stdin   (&_iob[0])
#define stdout  (&_iob[1])
#define stderr  (&_iob[2])

#define    feof(p)     (((p)->is_eof) == 1)
#define    ferror(p)   (((p)->is_err) == 1)
#define    fileno(p)   ((p)->fd)

int _fillbuf (FILE *);
int _flushbuf (int, FILE *);

#define getc(p)   (--(p)->cnt >= 0 ? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define putc(x,p) (--(p)->cnt >= 0 ? *(p)->ptr++ = (x) : _flushbuf((x),p))

#define getchar()   getc(stdin)
#define putchar(x)  putc((x), stdout)

FILE *fopen (char *, char *);
int fflush (FILE *);
int fclose (FILE *);
int fseek (FILE *, long, int);

#endif
