/* io_masks.h -- Implementation of standard library I/O functions where "masks"
   signifies bit fiddling with the help of bit masks. */

#ifndef IO_MASKS_H
#define IO_MASKS_H	1

#define EOF		(-1)
#define BUFSIZ		1024
#define OPEN_MAX	20	/* Max #files open at once. */
#define PERMS		0666	/* RW for owner, group, others */
#define SEEK_SET	0	/* Seek from beginning of file.  */
#define SEEK_CUR	1	/* Seek from current position.  */
#define SEEK_END	2	/* Seek from end of file.  */

enum _flags
{
  _READ = 01,			/* File open for reading. */
  _WRITE = 02,			/* File open for writing. */
  _UNBUF = 04,			/* File is unbuffered. */
  _EOF = 010,			/* EOF has occurred on this file. */
  _ERR = 020			/* Error occurred on this file. */
};

typedef struct _iobuf
{
  int cnt;			/* Characters left. */
  char *ptr;			/* Next character position. */
  char *base;			/* Location of buffer. */
  int flag;			/* Mode of file access. */
  int fd;			/* File descriptor. */
} FILE;

extern FILE _iob[OPEN_MAX];

#define stdin   (&_iob[0])
#define stdout  (&_iob[1])
#define stderr  (&_iob[2])

#define    feof(p)     (((p)->flag &_EOF) != 0)
#define    ferror(p)   (((p)->flag &_ERR) != 0)
#define    fileno(p)   ((p)->fd)

int _fillbuf (FILE *);
int _flushbuf (int, FILE *);

#define getc(p)   (--(p)->cnt >= 0 ? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define putc(x,p) (--(p)->cnt >= 0 ? *(p)->ptr++ = (x) : _flushbuf((x),p))

#define getchar()   getc(stdin)
#define putchar(x)  putc((x), stdout)

FILE *fopen (char *, char *);
int fflush (FILE *);
int fclose (FILE *);
int fseek (FILE *, long, int);

#endif
