/* io_masks.c -- Implementation of standard library I/O functions where "masks"
   signifies bit fiddling with the help of bit masks. */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "io_masks.h"

#define MASQ	-2		/* A masquerading character. */

FILE _iob[OPEN_MAX] = {		/* Stdin, Stdout, Stderr: */
  {0, (char *) 0, (char *) 0, _READ, 0},
  {0, (char *) 0, (char *) 0, _WRITE, 1},
  {0, (char *) 0, (char *) 0, _WRITE | _UNBUF, 2}
};

/* Allocate and fill input buffer. */
int
_fillbuf (FILE * fp)
{
  int bufsize;

  if ((fp->flag & (_READ | _EOF | _ERR)) != _READ)
    return EOF;

  bufsize = (fp->flag & _UNBUF) ? 1 : BUFSIZ;
  if (fp->base == NULL)		/* No buffer yet. */
    if ((fp->base = (char *) malloc (bufsize)) == NULL)
      return EOF;		/* Can't get buffer. */

  fp->ptr = fp->base;
  fp->cnt = read (fp->fd, fp->ptr, bufsize);

  if (--fp->cnt < 0)
    {
      if (fp->cnt == -1)
	fp->flag |= _EOF;
      else
	fp->flag |= _ERR;
      fp->cnt = 0;
      return EOF;
    }
  return (unsigned char) *fp->ptr++;
}

/* Allocate and flush input buffer. */
int
_flushbuf (int c, FILE * fp)
{
  int bufsize, nbytes;

  if ((fp->flag & (_WRITE | _EOF | _ERR)) != _WRITE)
    return EOF;

  bufsize = (fp->flag & _UNBUF) ? 1 : BUFSIZ;
  if (fp->base == NULL)		/* No buffer yet. */
    {
      if ((fp->base = (char *) malloc (bufsize)) == NULL)
	return EOF;		/* Can't get buffer. */
      else
	{
	  fp->ptr = fp->base;
	  fp->cnt = bufsize;
	}
    }

  if (c == EOF)
    fp->flag |= _EOF;
  else if (c != MASQ)
    {
      *fp->ptr++ = c;
      fp->cnt--;
    }

  nbytes = fp->ptr - fp->base;
  if (write (fp->fd, fp->base, nbytes) != nbytes)
    {
      fp->flag |= _ERR;
      return EOF;
    }

  fp->ptr = fp->base;
  fp->cnt = bufsize;
  return c;
}

/* Open a file and create a new stream for it. */
FILE *
fopen (char *name, char *mode)
{
  int fd;
  FILE *fp;

  if (*mode != 'r' && *mode != 'w' && *mode != 'a')
    return NULL;
  for (fp = _iob; fp < _iob + OPEN_MAX; fp++)
    if ((fp->flag & (_READ | _WRITE)) == 0)
      break;			/* Found free slot. */
  if (fp >= _iob + OPEN_MAX)	/* No free slots. */
    return NULL;

  if (*mode == 'w')
    fd = creat (name, PERMS);
  else if (*mode == 'a')
    {
      if ((fd = open (name, O_WRONLY, 0)) == -1)
	fd = creat (name, PERMS);
      lseek (fd, 0L, 2);
    }
  else
    fd = open (name, O_RDONLY, 0);
  if (fd == -1)			/* Couldn't access name. */
    return NULL;
  fp->fd = fd;
  fp->cnt = 0;
  fp->base = NULL;
  fp->flag = (*mode == 'r') ? _READ : _WRITE;
  return fp;
}

/* Flush FP, or all streams if FP is NULL. */
int
fflush (FILE * fp)
{
  int result = EOF;

  if (fp == NULL)
    for (fp = _iob; fp < _iob + OPEN_MAX; fp++)
      {
	if (fp->flag & _WRITE)
	  result = fflush (fp);
      }
  else if (fp->flag & _WRITE)
    result = _flushbuf (MASQ, fp);
  else if (fp->flag & _READ)	/* TODO 2022-07-28: Test it. */
    {
      fp->ptr = fp->base;
      fp->cnt = 0;
      result = 0;
    }
  return result != EOF ? 0 : EOF;
}

/* Close FP. */
int
fclose (FILE * fp)
{
  if (fp == NULL)
    return EOF;

  fflush (fp);
  free (fp->base);
  fp->base = NULL;
  fp->ptr = NULL;
  if (fp->fd <= 2)
    return 0;
  fp->flag &= ~(_READ | _WRITE);
  return close (fp->fd);
}

/* Seek to a certain position on FP. */
int
fseek (FILE * fp, long offset, int whence)
{
  if ((fp->flag & _UNBUF) == 0 && fp->base != NULL)
    {
      /* By: Gregory Pietsch
       * <https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_8:Exercise_4#Second_solution:>.
       * Fix offset so that it's from the last character the user read instead
       * of the last character that "actually" read. */
      if ((fp->flag & _READ) && whence == SEEK_CUR)
	{
	  if (offset < 0 || offset > fp->cnt)
	    offset -= fp->cnt;
	  else
	    {
	      fp->cnt -= offset;
	      fp->ptr += offset;
	      fp->flag &= ~_EOF;
	      return 0;
	    }
	}
      if (fflush (fp) == -1)
	return EOF;
    }
  if (lseek (fp->fd, offset, whence) == -1)
    {
      fp->flag |= _ERR;
      return EOF;
    }
  fp->flag &= ~_EOF;
  return 0;
}
