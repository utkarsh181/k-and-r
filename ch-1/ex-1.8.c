#include <stdio.h>

int
main (void)
{
  int c, nl, nt, nb;

  nl = nt = nb = 0;
  while ((c = getchar ()) != EOF)
    {
      if (c == ' ')
	++nb;
      else if (c == '\t')
	++nt;
      else if (c == '\n')
	++nl;
    }

  printf ("Number of newlines: %d\n", nl);
  printf ("Number of tabs: %d\n", nt);
  printf ("Number of blanks: %d\n", nb);

  return 0;
}
