#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i, j;

  /* By Richard:
   * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_16.
   * Earlier I was just catching error inside 'main'. */
  for (i = 0, j = 0; (c = getchar ()) != EOF && c != '\n'; ++i)
    {
      /* Ignore updating character array if limit is reached. */
      if (i < lim - 1)
	{
	  s[j] = c;
	  ++j;
	}
    }

  if (c == '\n')
    {
      if (i < lim)
	{
	  s[j] = c;
	  ++j;
	}
      ++i;
    }

  s[j] = '\0';

  return i;
}

/* Copy character array FROM to chracter array TO. */
void
copy (char to[], char from[])
{
  int i;

  i = 0;
  while ((to[i] = from[i]) != '\0')
    ++i;
}

int
main (void)
{
  int len;			/* Current line length */
  int max;			/* Maximum length seen so far */
  char line[MAXLINE];		/* Current input line */
  char longest[MAXLINE];	/* Longest line saved here */

  max = 0;
  while ((len = get_line (line, MAXLINE)) > 0)
    if (len > max)
      {
	max = len;
	copy (longest, line);
      }

  if (max > 0)
    printf ("Longest input of %d characters: \n%s", max, longest);

  return 0;
}
