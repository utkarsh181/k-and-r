/* ex-1.19.c -- Reverse input line */

#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i, j;

  /* By Richard:
   * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_16.
   * Earlier I was just catching error inside 'main'. */
  for (i = 0, j = 0; (c = getchar ()) != EOF && c != '\n'; ++i)
    {
      /* Ignore updating character array in limit is reached. */
      if (i < lim - 1)
	{
	  s[j] = c;
	  ++j;
	}
    }

  if (c == '\n')
    {
      if (i < lim)
	{
	  s[j] = c;
	  ++j;
	}
      ++i;
    }

  s[j] = '\0';

  return i;
}

/* Reverse character array S of length LEN. */
int
reverse (char s[], int len)
{
  char ch;
  int i, j;

  /* Swap elements */
  j = len - 1;
  for (i = 0; i < j; i++)
    {
      ch = s[i];
      s[i] = s[j];
      s[j] = ch;
      --j;
    }

  return 0;
}

int
main (void)
{
  int len;			/* Current line length */
  char line[MAXLINE];		/* Current input line */

  while ((len = get_line (line, MAXLINE)) > 0)
    {
      reverse (line, len);
      printf ("%s", line);
    }

  return 0;
}
