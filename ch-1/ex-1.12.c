#include <stdio.h>

#define IN   1			/* Inside a word */
#define OUT  0			/* Outside a word */

int
main (void)
{
  int c, state;

  state = OUT;
  while ((c = getchar ()) != EOF)
    {
      if (c == ' ' || c == '\n' || c == '\t')
	{
	  if (state == IN)
	    {
	      state = OUT;
	      putchar ('\n');
	    }
	}
      else
	{
	  state = IN;
	  putchar (c);
	}
    }

  return 0;
}
