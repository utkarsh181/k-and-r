/* ex-1.9.c -- Replace strings of one or more blanks by a single blank */

#include <stdio.h>

#define IN 1	/* Inside a blank */
#define OUT 0	/* Outside a blank */

int
main (void)
{
  int c, state;

  state = OUT;
  while ((c = getchar ()) != EOF)
    {
      if (c == ' ' && state == OUT)
	{
	  state = IN;
	  putchar (c);
	}
      else if (c != ' ')
	{
	  state = OUT;
	  putchar (c);
	}
    }

  return 0;
}
