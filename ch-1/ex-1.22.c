/* ex-1.22.c -- Fold long input lines into two or more shorter lines */

#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */
#define FOLD_COL 80		/* Maximum column after which lines are folded */

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i, j;

  /* By Richard:
   * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_16.
   * Earlier I was just catching error inside 'main'. */
  for (i = 0, j = 0; (c = getchar ()) != EOF && c != '\n'; ++i)
    {
      /* Ignore updating character array if limit is reached. */
      if (i < lim - 1)
	{
	  s[j] = c;
	  ++j;
	}
    }

  if (c == '\n')
    {
      if (i < lim)
	{
	  s[j] = c;
	  ++j;
	}
      ++i;
    }

  s[j] = '\0';

  return i;
}

/* Fold line LINE in range [start, end), with element line[start] at column
 * COLUMN. */
void
fold_line (char line[], int start, int end, int column)
{
  if (start < end)
    {
      if (column > FOLD_COL && (line[start] == ' ' || line[start] == '\t'))
	{
	  column = 0;
	  putchar ('\n');
	}
      else
	{
	  ++column;
	  putchar (line[start]);
	}

      fold_line (line, start + 1, end, column);
    }
}

int
main (void)
{
  int len;			/* Current line length */
  char line[MAXLINE];		/* Current input line */

  while ((len = get_line (line, MAXLINE)) > 0)
    fold_line (line, 0, len, 0);

  return 0;
}
