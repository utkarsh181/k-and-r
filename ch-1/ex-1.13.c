/* ex-1.13.c -- Histogram of length of words in input */

#include <stdio.h>

#define OUT  0			/* Outside a word */
#define IN   1			/* Inside a word */
#define DONE 2			/* At EOF */

#define MAX_WORD_LEN 10		/* Maximum frequency for word length */

int
main (void)
{
  int i, c, state;
  long j, len;

  /* FIXME 2021-11-07: Minimum word length is 1, but we are storing it at
   * index 0.  Is it possible to circumvent this? */
  long word_len_freq[MAX_WORD_LEN + 1];

  for (i = 0; i <= MAX_WORD_LEN; ++i)
    word_len_freq[i] = 0;

  len = 0;
  state = OUT;
  while (state != DONE)
    {
      c = getchar ();

      if (c == ' ' || c == '\n' || c == '\t' || c == EOF)
	{
	  /* When STATE is OUT, do nothing.  But when it is IN,
	   * update the respective frequencies. */
	  if (state == IN)
	    {
	      state = OUT;

	      if (len > MAX_WORD_LEN)
		++word_len_freq[MAX_WORD_LEN];
	      else
		++word_len_freq[len];
	    }
	  if (c == EOF)
	    state = DONE;
	}
      /* Update LEN and STATE when a new word in encountered */
      else if (state == OUT)
	{
	  state = IN;
	  len = 0;
	}
      else
	++len;
    }

  /* Print histogram */
  printf ("Word length histogram:\n");
  for (i = 0; i <= MAX_WORD_LEN; ++i)
    {
      /* TODO 2021-11-07: Don't hardcode width, use MAX_WORD_LEN
       * to find appropriate width. */
      if (i == MAX_WORD_LEN)
	printf (">%2d: ", MAX_WORD_LEN);
      else
	printf ("%3d: ", i + 1);

      for (j = 0; j < word_len_freq[i]; ++j)
	printf ("*");
      printf ("\n");
    }

  return 0;
}
