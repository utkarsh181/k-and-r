/* ex-1.14.c -- Histogram for characters in input */

#include <stdio.h>
#include <limits.h>
#include <ctype.h>

int
main (void)
{
  int i, c;
  long j;
  long char_freq[CHAR_MAX + 1];	/* TODO 2021-11-08: Can we support Unicode? */

  for (i = 0; i <= CHAR_MAX; ++i)
    char_freq[i] = 0;

  while ((c = getchar ()) != EOF)
    {
      if (c < CHAR_MAX)
	++char_freq[c];
      else
	++char_freq[CHAR_MAX];
    }

  printf ("Input charaters histogram:\n");
  for (i = 0; i <= CHAR_MAX; ++i)
    {
      if (isprint (i))
	{
	  printf ("%3d: ", i);

	  for (j = 0; j < char_freq[i]; ++j)
	    printf ("*");
	  printf ("\n");
	}
    }

  return 0;
}
