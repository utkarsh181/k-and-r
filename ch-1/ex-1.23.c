/* ex-1.23.c -- Uncomment a C program. */

/* REVIEW 2021-12-25: Checkout solutions at:
   https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_23 */

#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */

/* Different state for the uncomment machine. */
enum states { OUT, IN_COMMENT, IN_STRING };

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i, j;

  /* By Richard:
   * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_16.
   * Earlier I was just catching error inside 'main'. */
  for (i = 0, j = 0; (c = getchar ()) != EOF && c != '\n'; ++i)
    {
      /* Ignore updating character array if limit is reached. */
      if (i < lim - 1)
	{
	  s[j] = c;
	  ++j;
	}
    }

  if (c == '\n')
    {
      if (i < lim)
	{
	  s[j] = c;
	  ++j;
	}
      ++i;
    }

  s[j] = '\0';

  return i;
}

/* Uncomment a line LINE with maximum length LEN.  PRE_STATE denotes the state
   of the line previous to current line. */
int
uncomment_line (char line[], int len, int pre_state)
{
  int i, state;

  state = pre_state;
  for (i = 0; i < len; i++)
    {
      if (line[i] == 34 && state != IN_COMMENT) /* Double quote */
	{
	  if (state == OUT)
	    state = IN_STRING;
	  else
	    state = OUT;

	  putchar (line[i]);
	}
      else if (line[i] == '/')
	{
	  if (line[i+1] == '*' && state == OUT)
	    {
	      state = IN_COMMENT;
	      i += 1;
	    }
	  else if (state != IN_COMMENT)
	    putchar (line[i]);
	}
      else if (line[i] == '*')
	{
	  if (line[i+1] == '/' && state == IN_COMMENT)
	    {
	      state = OUT;
	      i += 1;
	    }
	  else if (state != IN_COMMENT)
	    putchar (line[i]);
	}
      else if (state != IN_COMMENT)
	  putchar (line[i]);
    }

  return state;
}

int
main (void)
{
  int len;			/* Current line length */
  char line[MAXLINE];		/* Current input line */
  int state;			/* Current state */

  state = OUT;
  while ((len = get_line (line, MAXLINE)) > 0)
      state = uncomment_line (line, len, state);

  return 0;
}
