/* entab.c -- Replace strings of blanks by minimum number of tabs and blanks */

#include <stdio.h>

#define MAXLINE 1000		/* Maximum input line */

/* Distance between tab stops (for display of tab characters), in columns. */
int tab_width = 8;

/* Numeric value of leftmost/first column */
int first_column = 0;

/* Get a line in character array S with maximum limit LIM. */
int
get_line (char s[], int lim)
{
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar ()) != EOF && c != '\n'; ++i)
    s[i] = c;

  if (c == '\n')
    {
      s[i] = c;
      ++i;
    }

  s[i] = '\0';

  return i;
}

/* Is character S is reating in character array S from START to END. */
int
is_repeating (char s[], char c, int start, int end)
{
  while (start < end)
    {
      if (s[start] != c)
	return 0;

      ++start;
    }

  return 1;
}

int
next_tabstop (int column)
{
  return tab_width * (column / tab_width + 1);
}

void
print_line (char line[], int len)
{
  int i, c, column, tmp;

  i = 0;
  column = first_column;
  while (i < len)
    {
      c = line[i];

      if (c == '\n')
	{
	  ++i;
	  column = first_column;
	  putchar (c);
	}
      else if (c == ' ')
	{
	  tmp = next_tabstop (column);

	  if (is_repeating (line, ' ', column, tmp))
	    {
	      i += tmp - column;
	      column = tmp;
	      putchar ('\t');
	    }
	  else
	    {
	      ++i;
	      ++column;
	      putchar (c);
	    }
	}
      else
	{
	  ++i;
	  ++column;
	  putchar (c);
	}
    }
}

int
main (void)
{
  int len;			/* Current line length */
  char line[MAXLINE];		/* Current input line */

  while ((len = get_line (line, MAXLINE)) > 0)
    {
      print_line (line, len);
    }

  return 0;
}
