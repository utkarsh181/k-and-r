#include <stdio.h>

#define LOWER 0			/* Lower limit of temperature table. */
#define UPPER 300		/* Upper limit */
#define STEP 20			/* Step size */

float
fahr2celsius (float fahr)
{
  return (5.0 / 9.0) * (fahr - 32.0);
}

float
celsius2fahr (float celsius)
{
  return (9.0 / 5.0) * celsius + 32.0;
}

int
main ()
{
  float fahr, celsius;

  printf ("Fahrenheit Celsius\n");
  for (fahr = LOWER; fahr <= UPPER; fahr += STEP)
    {
      printf ("%3.0f %13.2f\n", fahr, fahr2celsius (fahr));
    }
  printf ("\n");

  printf ("Celsius Fahrenheit\n");
  for (celsius = LOWER; celsius <= UPPER; celsius += STEP)
    {
      printf ("%3.0f %7.0f\n", celsius, celsius2fahr (celsius));
    }
}
