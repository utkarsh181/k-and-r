/* detab.c -- Replaces tabs in input with blanks */

#include <stdio.h>

/* Distance between tab stops (for display of tab characters), in columns. */
int tab_width = 8;

/* Numeric value of leftmost/first column */
int first_column = 0;

int
next_tabstop (int column)
{
  return tab_width * (column / tab_width + 1);
}

int
main (void)
{
  int i, c, tmp, column;

  column = first_column;
  while ((c = getchar ()) != EOF)
    {
      if (c == '\n')
	{
	  column = first_column;
	  putchar (c);
	}
      else if (c == '\t')
	{
	  tmp = next_tabstop (column);

	  while (column < tmp)
	    {
	      ++column;
	      putchar (' ');
	    }
	}
      else
	{
	  ++column;
	  putchar (c);
	}
    }

  return 0;
}
