#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

#define MAX_INT_WIDTH 34

/* Helper function for itoa. */
int
itoa_iter (int n, char *s, int b, int i)
{
  char digit[] = "0123456789abcdefghijklmnopqrstuvwxyz";

  if (b > sizeof (digit) / sizeof (char) || b < 2)
    {
      fprintf (stderr, "error (itoa_iter): invalid base\n");
      exit (EXIT_FAILURE);
    }

  if (n / b)
    i = itoa_iter (n / b, s, b, i);

  s[i] = digit[abs (n % b)];
  return i+1;
}

/* Convert integer N in base B to character string S. */
void
itoa (int n, char *s, int b)
{
  int i = 0;

  if (n < 0)
    s[i++] = '-';
  i = itoa_iter (n, s, b, i);
  s[i] = '\0';
}

int
main (void)
{
  char buffer[MAX_INT_WIDTH];

  itoa (INT_MAX, buffer, 10);
  printf ("%s\n", buffer);

  return 0;
}
