/* rpcal.c -- Calculator with reverse Polish notation.

  Copyright (C) 2022 Utkarsh Singh

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>. */

/* Commentary:

   This program implements a calculator with reverse Polish notation, in which
   each operator follows its operands; an expression like

   (1 - 2) * (4 + 5)

   is entered as

   1 2 - 4 5 + *

   Parentheses are not needed; the notation is unambiguous as long as know how
   many operands each operator expects.*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include "rpcal.h"

#define MAXOP	100		/* Max size of operand or operator. */

/* Evaluate identifier S. */
void
eval_identifier (const char *s)
{
  double op2;

  if (strcmp (s, "sin") == 0)
    push (sin (pop ()));
  else if (strcmp (s, "cos") == 0)
    push (cos (pop ()));
  else if (strcmp (s, "tan") == 0)
    push (tan (pop ()));
  else if (strcmp (s, "exp") == 0)
    push (exp (pop ()));
  else if (strcmp (s, "pow") == 0)
    {
      op2 = pop ();
      push (pow (pop (), op2));
    }
  else
    printf ("error (eval_identifier): unknow identifier");
}

int
main (void)
{
  int types;
  double op2;
  char s[MAXOP];

  while ((types = getop (s)) != EOF)
    {
      switch (types)
	{
	case NUMBER:
	  push (atof (s));
	  break;
	case IDENTIFIER:
	  eval_identifier (s);
	  break;
	case '+':
	  push (pop () + pop ());
	  break;
	case '*':
	  push (pop () * pop ());
	  break;
	case '-':
	  op2 = pop ();
	  push (pop () - op2);
	  break;
	case '/':
	  op2 = pop ();
	  if (op2 != 0.0)
	    push (pop () / op2);
	  else
	    printf ("error (main): zero divisor\n");
	  break;
	case '%':
	  op2 = pop ();
	  if (op2 != 0.0)
	    push ((int) pop () % (int) op2);
	  else
	    printf ("error (main): zero divisor\n");
	  break;

	  /* FIXME 2021-12-31: Every '\n' leads to a pop, how can we even put a
	     element onto the stack without performing without pop-ing it? */
	case '?':
	  print_tos ();
	  break;
	case '!':
	  clear_stack ();
	  break;
	case '#':
	  duplicate_tos ();
	  break;
	case '~':
	  swap_tos ();
	  break;
	case '\n':
	  printf ("\t%.8g\n", pop ());
	  break;

	default:
	  printf ("error (main): unknown command %s\n", s);
	  break;
	}
    }
  return 0;
}
