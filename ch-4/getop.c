#include <stdio.h>
#include <ctype.h>
#include "rpcal.h"

/* Get next operator or numeric operand. */
int
getop (char *s)
{
  int i, c;

  /* Parse operator. */
  while ((s[0] = c = getch ()) == ' ' || c == '\t')
    ;
  if (c == '-' || c == '+')
    {
      s[1] = getch ();
      if (!isdigit (s[1]))
	{
	  ungetch (s[1]);
	  s[1] = '\0';
	  return c;
	}
    }
  else if (!isalnum (c) && c != '.')
    {
      s[1] = '\0';
      return c;
    }

  i = 0;
  if (c == '-' || c == '+')
    c = s[++i];

  /* Parse identifier */
  if (isalpha (c))
    {
      while (isalpha (s[++i] = c = getch ()))
	;
      s[i] = '\0';
      if (c != EOF)
	ungetch (c);
      return IDENTIFIER;
    }

  /* Parse number */
  if (isdigit (c))
    while (isdigit (s[++i] = c = getch ()))
      ;
  if (c == '.')
    while (isdigit (s[++i] = c = getch ()))
      ;
  s[i] = '\0';
  if (c != EOF)
    ungetch (c);
  return NUMBER;
}
