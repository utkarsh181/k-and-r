#include <stdio.h>
#include <ctype.h>

/* Convert string S to double. */
double
atof (const char *s)
{
  double val, power;
  int i, sign;
  int exponent, esign;

  /* Parse significand */
  for (i = 0; isspace (s[i]); i++)
    ;
  sign = (s[i] == '-') ? -1 : 1;
  if (s[i] == '+' || s[i] == '-')
    i++;
  for (val = 0.0; isdigit (s[i]); i++)
    val = 10.0 * val + (s[i] - '0');
  if (s[i] == '.')
    i++;
  for (power = 1.0; isdigit (s[i]); i++)
    {
      val = 10.0 * val + (s[i] - '0');
      power *= 10.0;
    }

  /* Parse exponent */
  if (s[i] == 'e' || s[i] == 'E')
    {
      i++;
      esign = (s[i] == '-') ? -1 : 1;
      if (s[i] == '+' || s[i] == '-')
	i++;
      for (exponent = 0; isdigit (s[i]); i++)
	exponent = 10 * exponent + (s[i] - '0');
      if (esign < 0)
	{
	  while (exponent)
	    {
	      power *= 10.0;
	      --exponent;
	    }
	}
      else
	{
	  while (exponent)
	    {
	      power /= 10.0;
	      --exponent;
	    }
	}
    }
  return sign * val / power;
}

int
main (void)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  while ((nread = getline (&line, &len, stdin)) != -1)
    printf ("%g\n", atof (line));

  return 0;
}
