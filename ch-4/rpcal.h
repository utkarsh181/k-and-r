#ifndef RPCAL_H
#define RPCAL_H	1

#define NUMBER	'0'		/* Signal that a number was found. */
#define IDENTIFIER '1'		/* Signal that an identifier (ie. a math.h
				   function) was found. */

void push (double);		/* Push value to stack. */
double pop (void);		/* Pop value from stack. */
void print_tos (void);		/* Print Top Of Stack. */
void duplicate_tos (void);	/* Duplicate Top Of Stack. */
void swap_tos (void);		/* Swap Top Of Stack. */
void clear_stack (void);	/* Clear stack. */

int getch (void);		/* Get a (possibly pushed back) character. */
void ungetch (int);		/* Push character back on input. */

int getop (char *);		/* Get next operator or numeric operand. */

#endif
