#include <stdio.h>
#include <string.h>

/* Helper function for reverse. */
void
reverse_iter (char *s, size_t i, size_t j)
{
  char c;

  if (i < j)
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
      reverse_iter (s, i + 1, j - 1);
    }
}

/* Reverse S of length LEN. */
void
reverse (char *s, size_t len)
{
  if (len > 1)
      reverse_iter (s, 0, len - 1);
}

int
main (void)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  while ((nread = getline (&line, &len, stdin)) != -1)
    {
      reverse (line, nread);
      printf ("%s", line);
    }
  return 0;
}
