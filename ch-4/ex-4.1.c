#include <stdio.h>
#include <string.h>

/* Return rightmost occurance of T in S. */
int
strrindex (char *s, char *t)
{
  int i, j, k;
  int n1 = strlen (s) - 1, n2 = strlen (t) - 1;

  for (i = n1; i >= 0; i--)
    {
      for (j = i, k = n2; k >= 0 && s[j] == t[k]; j--, k--)
	;
      if (k == -1)
	return j + 1;
    }
  return -1;
}

int
main (void)
{
  char s[] = "cqabcdef";
  char t[] = "ef";
  char u[] = "cd";
  char v[] = "cqa";
  char z[] = "gh";
  int i;

  if ((i = strrindex (s, t)) >= 0)
    printf ("'%s' matches '%s' at index %d\n", t, s, i);
  else
    printf ("'%s' doesn't match '%s' at all\n", t, s);

  if ((i = strrindex (s, u)) >= 0)
    printf ("'%s' matches '%s' at index %d\n", u, s, i);
  else
    printf ("'%s' doesn't match '%s' at all\n", u, s);

  if ((i = strrindex (s, v)) >= 0)
    printf ("'%s' matches '%s' at index %d\n", v, s, i);
  else
    printf ("'%s' doesn't match '%s' at all\n", v, s);

  if ((i = strrindex (s, z)) >= 0)
    printf ("'%s' matches '%s' at index %d\n", z, s, i);
  else
    printf ("'%s' doesn't match '%s' at all\n", z, s);

  return 0;
}
