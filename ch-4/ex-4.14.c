#include <stdio.h>

/* By Appoorva Anand:
  <https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_4:Exercise_14>.  Earlier I
  was just ignoring authors comment on using block structure.

  Replacement text is wrapped under braces to ensure limted scope of TMP.  In
  this case, this will help in subsequent call of swap with different types of
  formal arguments.*/
#define swap(t, x, y) {t tmp = x; x = y; y = tmp;}

/* Print 2 variable X and Y. */
#define print2var(x, y) printf (#x " = %g, " #y " = %g\n", x, y);

int
main (void)
{
  double a = 100.5, b = 200.9;
  float p = 3.03, q = 4.04;

  printf ("Before swap:\n");
  print2var (a, b);
  print2var (p, q);
  swap (double, a, b);
  swap (float, p, q);
  printf ("After swap:\n");
  print2var (a, b);
  print2var (p, q);
  return 0;
}
