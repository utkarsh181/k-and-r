.POSIX :
CC	= cc
CFLAGS	= -W -O
LDLIBS	= -lm
PREFIX	= /usr/local
RPCAL_OBJECTS	= ch-4/rpcal.o ch-4/getch.o ch-4/stack.o ch-4/getop.o
RPEXPR_OBJECTS	= ch-5/rpexpr.o ch-5/stack.o

all : ch-1/hello ch-4/rpcal ch-5/rpexpr ch-5/usort ch-5/dcl \
      ch-7/upcase ch-7/downcase

install : ch-4/rpcal ch-5/rpexpr ch-5/usort ch-5/dcl \
	  ch-7/upcase ch-7/downcase
	mkdir -p $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-4/rpcal $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-5/rpexpr $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-5/usort $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-5/dcl $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-7/upcase $(DESTDIR)/$(PREFIX)/bin
	cp -f ch-7/downcase $(DESTDIR)/$(PREFIX)/bin

uninstall : ch-4/rpcal ch-5/rpexpr ch-5/usort ch-5/dcl \
	    ch-7/upcase ch-7/downcase
	rm -f $(DESTDIR)$(PREFIX)/bin/rpcal
	rm -f $(DESTDIR)$(PREFIX)/bin/rpexpr
	rm -f $(DESTDIR)$(PREFIX)/bin/usort
	rm -f $(DESTDIR)/$(PREFIX)/bin/dcl
	rm -f $(DESTDIR)/$(PREFIX)/bin/upcase
	rm -f $(DESTDIR)/$(PREFIX)/bin/downcase

ch-1/hello : ch-1/hello.o
	$(CC) $(LDFLAGS) -o ch-1/hello ch-1/hello.o

ch-4/rpcal : $(RPCAL_OBJECTS)
	$(CC) $(LDFLAGS) -o ch-4/rpcal $(RPCAL_OBJECTS) $(LDLIBS)
ch-4/rpcal.o : ch-4/rpcal.h
ch-4/stack.o : ch-4/rpcal.h
ch-4/getop.o : ch-4/rpcal.h

ch-5/rpexpr : $(RPEXPR_OBJECTS)
	$(CC) $(LDFLAGS) -o ch-5/rpexpr $(RPEXPR_OBJECTS) $(LDLIBS)
ch-5/rpexpr.o : ch-5/stack.h

ch-5/usort : ch-5/usort.o
	$(CC) $(LDFLAGS) -o ch-5/usort ch-5/usort.o $(LDLIBS)

ch-5/dcl : ch-5/dcl.o ch-5/getch.o
	$(CC) $(LDFLAGS) -o ch-5/dcl ch-5/dcl.o ch-5/getch.o $(LDLIBS)
ch-5/dcl.o : ch-5/getch.h

ch-7/upcase : ch-7/ex-7.1.o
	$(CC) $(LDFLAGS) -o ch-7/upcase ch-7/ex-7.1.o

ch-7/downcase : ch-7/ex-7.1.o
	$(CC) $(LDFLAGS) -o ch-7/downcase ch-7/ex-7.1.o

.PHONY : clean
clean :
	rm -f ch-1/hello ch-1/hello.o
	rm -f ch-4/rpcal $(RPCAL_OBJECTS)
	rm -f ch-5/rpexpr $(RPEXPR_OBJECTS)
	rm -f ch-5/usort ch-5/usort.o
	rm -f ch-5/dcl ch-5/dcl.o ch-5/getch.o
	rm -f ch-7/ex-7.1.o ch-7/upcase ch-7/downcase
