#include <stdio.h>
#include <string.h>
#include <ctype.h>

int
main (int argc, char *argv[])
{
  if (argc != 1)
    {
      fprintf (stderr, "error: invalid number of arguments.\n");
      return 1;
    }

  int c;

  if (strcmp (argv[0], "upcase") == 0)
    {
      while ((c = getchar ()) != EOF)
	putchar (toupper (c));
    }
  else if (strcmp (argv[0], "downcase") == 0)
    {
      while ((c = getchar ()) != EOF)
	putchar (tolower (c));
    }
  else
    {
      fprintf (stderr, "error: useless program name.\n");
      return 1;
    }
  return 0;
}
