#include <stdio.h>
#include <stdarg.h>

/* TODO 2022-01-18: Add support of 'h' and 'l' size modifiers. */

/* Minimized version of printf. */
void
minprintf (char *fmt, ...)
{
  va_list ap;
  char *p, *sval;
  int ival;
  double dval;

  va_start (ap, fmt);
  for (p = fmt; *p; p++)
    {
      if (*p != '%')
	{
	  putchar (*p);
	  continue;
	}
      switch (*++p)
	{
	case 'd':
	case 'i':
	  ival = va_arg (ap, int);
	  printf ("%d", ival);
	  break;
	case 'o':
	  ival = va_arg (ap, int);
	  printf ("%o", ival);
	  break;
	case 'x':
	case 'X':
	  ival = va_arg (ap, int);
	  printf ("%x", ival);
	  break;
	case 'u':
	  ival = va_arg (ap, int);
	  printf ("%u", ival);
	  break;
	case 'c':
	  ival = va_arg (ap, int);
	  printf ("%c", ival);
	  break;
	case 's':
	  for (sval = va_arg (ap, char *); *sval; sval++)
	    putchar (*sval);
	  break;
	case 'f':
	  dval = va_arg (ap, double);
	  printf ("%f", dval);
	  break;
	case 'e':
	case 'E':
	  dval = va_arg (ap, double);
	  printf ("%e", dval);
	  break;
	case 'g':
	case 'G':
	  dval = va_arg (ap, double);
	  printf ("%g", dval);
	  break;
	default:
	  putchar (*p);
	  break;
	}
    }
  va_end (ap);
}

int
main (void)
{
  minprintf ("My favourite %s is %c\n", "char", '@');
  minprintf ("My favourite %s is %d\n", "int", 73);
  minprintf ("My favourite %s is %f\n", "double", 7.3);
  minprintf ("My favourite %s is %s\n", "string", "seven");
  minprintf ("My favourite %s is %u\n", "unsigned int", -3);
  return 0;
}
