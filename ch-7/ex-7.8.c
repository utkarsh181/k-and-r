#include <stdio.h>
#include <limits.h>

int
main (int argc, char *argv[])
{
  FILE *fp;
  char line[LINE_MAX];
  int pc;			/* Page count. */

  if (argc < 1)
    {
      fprintf (stderr, "Usage: %s [FILE ...]\n", *argv);
      return 1;
    }

  pc = 1;
  while (--argc > 0 && ++argv)
    {
      if ((fp = fopen (*argv, "r")) == NULL)
	{
	  fprintf (stderr, "error: can't open %s\n", *argv);
	  return 2;
	}

      printf ("title: %s\npage: %d\n", *argv, pc);
      while (fgets (line, LINE_MAX, fp))
	printf ("%s", line);
      printf ("\f\n");
    }
  return 0;
}
