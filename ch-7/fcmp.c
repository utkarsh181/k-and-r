/* fcmp.c -- Compare two files, printing the first line where they differ. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void
fcmp (FILE * fp1, FILE * fp2)
{
  char buf1[LINE_MAX], buf2[LINE_MAX];

  while (fgets (buf1, LINE_MAX, fp1) && fgets (buf2, LINE_MAX, fp2))
    {
      if (strcmp (buf1, buf2) != 0)
	{
	  printf ("-%s+%s", buf1, buf2);
	  exit (3);
	}
    }
}

int
main (int argc, char *argv[])
{
  FILE *fp[2];
  char *prog = argv[0];

  if (argc != 3)
    {
      fprintf (stderr, "%s: wrong number of arguments\n", prog);
      exit (1);
    }
  else
    {
      while (--argc > 0)
	{
	  if ((fp[argc - 1] = fopen (*++argv, "r")) == NULL)
	    {
	      fprintf (stderr, "%s: can't open %s\n", prog, *argv);
	      exit (2);
	    }
	}
      fcmp (fp[0], fp[1]);
      fclose (fp[0]);
      fclose (fp[1]);
    }
  exit (0);
}
