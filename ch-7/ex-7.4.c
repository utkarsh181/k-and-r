#include <stdio.h>
#include <stdarg.h>
#include <limits.h>

/* TODO 2022-01-18: Return numbers of "matched" inputs, that is, number of
   inputs items succesfully matched and assigned.*/

/* Minimized version of scanf. */
void
minscanf (char *fmt, ...)
{
  va_list ap;
  char *p;
  int *iptr;
  unsigned *uiptr;
  char *cptr, *sptr;
  double *dptr;

  va_start (ap, fmt);
  for (p = fmt; *p; p++)
    {
      if (*p != '%')
	{
	  putchar (*p);
	  continue;
	}
      switch (*++p)
	{
	case 'd':
	case 'i':
	  iptr = va_arg (ap, int *);
	  scanf ("%d", &iptr);
	  break;
	case 'o':
	  iptr = va_arg (ap, int *);
	  scanf ("%o", &iptr);
	  break;
	case 'x':
	case 'X':
	  iptr = va_arg (ap, int *);
	  scanf ("%x", &iptr);
	  break;
	case 'u':
	  uiptr = va_arg (ap, unsigned *);
	  scanf ("%u", &uiptr);
	  break;
	case 'c':
	  cptr = va_arg (ap, char *);
	  scanf ("%c", &cptr);
	  break;
	case 's':
	  sptr = va_arg (ap, char *);
	  scanf ("%s", sptr);
	  break;
	case 'f':
	  dptr = va_arg (ap, double *);
	  scanf ("%f", dptr);
	  break;
	case 'e':
	case 'E':
	  dptr = va_arg (ap, double *);
	  scanf ("%e", &dptr);
	  break;
	case 'g':
	case 'G':
	  dptr = va_arg (ap, double *);
	  scanf ("%g", &dptr);
	  break;
	}
    }
  va_end (ap);
}

/* TODO 2022-01-18: Write some test. */
int
main (void)
{
  int ival;
  double dval;
  char sval[LINE_MAX];

  printf ("Enter an integer value: ");
  minscanf ("%d", &ival);
  return 0;
}
