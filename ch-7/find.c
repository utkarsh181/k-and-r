/* find.c -- Find the lines that matches pattern. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

int
find (const char *pathname, const char *pattern, int number, int except)
{
  FILE *fp;
  char line[LINE_MAX];
  unsigned found = 0;
  unsigned long lineno = 0;

  if (!pathname)
    fp = stdin;
  else if ((fp = fopen (pathname, "r")) == NULL)
    {
      fprintf (stderr, "error: can't open %s\n", pathname);
      exit (-2);
    }

  while (fgets (line, LINE_MAX, fp))
    {
      lineno++;
      if ((strstr (line, pattern) != NULL) != except)
	{
	  if (pathname)
	    printf ("%s: ", pathname);

	  if (number)
	    printf ("%ld:", lineno);
	  printf ("%s", line);
	  found++;
	}
    }
  fclose (fp);
  return found;
}


int
main (int argc, char *argv[])
{
  char *pattern;
  int c, except = 0, number = 0, found = 0;

  while (--argc > 0 && (*++argv)[0] == '-')
    {
      while (c = *++argv[0])
	switch (c)
	  {
	  case 'x':
	    except = 1;
	    break;
	  case 'n':
	    number = 1;
	    break;
	  default:
	    fprintf (stderr, "find: illegal option %c\n", c);
	    argc = 0;
	    found = -1;
	    break;
	  }
    }
  if (argc < 1)
    fprintf (stderr, "Usage: find -x -n PATTERN [FILE ...]\n");
  else
    {
      pattern = *argv;
      found = 0;

      if (argc == 1)
	found += find (NULL, pattern, number, except);
      else
	{
	  while (--argc > 0)
	    found += find (*++argv, pattern, number, except);
	}
    }
  return found;
}
