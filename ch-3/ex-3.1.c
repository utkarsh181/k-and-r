#include <stdio.h>

/* FIXME 2021-12-28: This doesn't work after 21 for both binsearch and
   binsearch2. */
#define SIZE 1 << 21

int
binsearch (int x, int *v, int n)
{
  int low, mid, high;

  low = 0;
  high = n - 1;
  while (low <= high)
    {
      mid = (low + high) / 2;
      if (x < v[mid])
	high = mid - 1;
      else if (x > v[mid])
	low = mid + 1;
      else
	return mid;
    }
  return -1;
}

int
binsearch2 (int x, int *v, int n)
{
  int low, mid, high;

  /* NOTE 2021-12-28: Another possible solution by Paul Griffiths:
     https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_3:Exercise_1

     But I still prefer this solution as even though syntactically similar, it
     is much more in-efficient than author's solution.  And might implicitly
     represents difference between a novice and experienced C programmer. */

  low = 0;
  high = n - 1;
  while (low < high)
    {
      mid = (low + high) / 2;
      if (x <= v[mid])
	high = mid;
      else
	low = mid + 1;
    }
  return (v[low] == x) ? low : -1;
}

int
main (void)
{
  int i, n = SIZE;
  int values[n];

  for (i = 0; i < n; i++)
    values[i] = i;

  /* Search for every item including -1 */
  for (i = -1; i < n; i++)
    binsearch (i, values, n);

  return 0;
}
