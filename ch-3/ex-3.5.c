#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

#define MAX_INT_WIDTH 34

void
reverse (char *s)
{
  int c, i, j;

  for (i = 0, j = strlen (s) - 1; i < j; i++, j--)
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}


/* Convert N to character in S using base B. */
void
itoa (int n, char *s, int b)
{
  int i;
  int sign = n;			/* Record sign */

  char digit[] = "0123456789abcdefghijklmnopqrstuvwxyz";

  if (b > sizeof (digit) / sizeof (char) || b < 2)
    {
      fprintf (stderr, "error (itoa): invalid base\n");
      exit (EXIT_FAILURE);
    }

  i = 0;
  do				/* Generate digits in reverse order. */
    {
      s[i++] = digit[abs (n % b)];
    }
  while (n /= b);

  if (sign < 0)
    s[i++] = '-';

  s[i] = '\0';
  reverse (s);
}

int
main (void)
{
  char buffer[MAX_INT_WIDTH];

  itoa (255, buffer, 16);
  printf ("%s\n", buffer);

  return 0;
}
