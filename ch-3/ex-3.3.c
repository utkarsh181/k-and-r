#include <stdio.h>
#include <ctype.h>
#include <limits.h>

/* Can first character FC and last character LC be used to form a shorthand
   notation: "FC-LC"." */
int
isrange(int fc, int lc)
{
  return (isalpha (fc) && isalpha (lc)) || (isdigit (fc) && isdigit (lc));
}

/* Expands shorthand notations like "a-z" in the S1 into equivalent complete
   list "abc...xyz" in S2. */
void
expand (char *s1, char *s2)
{
  int i, j, k;

  for (i = 0, j = 0; s1[i] != '\0'; i++)
    {
      if (s1[i] == '-')
	{
	  if (i != 0 && isrange (s1[i-1], s1[i+1]))
	    {
	      j--;		/* Overwrite previous character */

	      /* Expand */
	      for (k = s1[i-1]; k <= s1[i+1]; k++)
		s2[j++] = k;

	      i++;		/* Ignore next character */
	    }
	  else
	    s2[j++] = '-';
	}
      else
	s2[j++] = s1[i];
    }
  s2[j] = '\0';
}

int
main (void)
{
  char *line = NULL;
  char buffer[LINE_MAX];
  size_t len = 0;
  ssize_t nread;

  while ((nread = getline (&line, &len, stdin)) != -1)
    {
      expand (line, buffer);
      printf ("%s", buffer);
    }

  return 0;
}
