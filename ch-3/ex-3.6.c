#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

void
reverse (char *s)
{
  int c, i, j;

  for (i = 0, j = strlen (s) - 1; i < j; i++, j--)
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}


/* Convert N to character in S with minimum width W. */
void
itoa (int n, char *s, int w)
{
  int i;
  int sign = n;			/* Record sign */

  i = 0;
  do
    {
      s[i++] = abs (n % 10) + '0';
    }
  while (n /= 10);

  if (sign < 0)
    s[i++] = '-';

  while (i < w)
    s[i++] = ' ';

  s[i] = '\0';
  reverse (s);
}

int
main (void)
{
  char buffer[LINE_MAX];

  itoa (INT_MIN, buffer, 13);
  printf ("%s\n", buffer);
}
