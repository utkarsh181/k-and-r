#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

#define MAX_INT_WIDTH 12

void
reverse (char *s)
{
  int c, i, j;

  for (i = 0, j = strlen (s) - 1; i < j; i++, j--)
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}


/* Convert N to character in S. */
void
itoa (int n, char *s)
{
  int i;
  int sign = n;			/* Record sign */

  i = 0;
  do
    {
      s[i++] = abs (n % 10) + '0';
    }
  while (n /= 10);

  if (sign < 0)
    s[i++] = '-';

  s[i] = '\0';
  reverse (s);
}

int
main (void)
{
  char buffer[MAX_INT_WIDTH];

  itoa (INT_MIN, buffer);
  printf ("%s\n", buffer);
}
