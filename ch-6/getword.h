#ifndef GETWORD_H
#define GETWORD_H

#define MAXWORD	100		/* Maximum length of a word. */

int getword (char *, int, FILE *); /* Get WORD with maximum limit LIM from STREAM. */
int iscomment (char *);		/* Is S a C comment. */
int isstringconstant (char *);	/* Is S a string constant. */

#endif
