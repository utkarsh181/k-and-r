#include <stdio.h>
#include <string.h>
#include "getwords.h"

/* TODO 2022-01-12: The main road blocker for this problem is: how you will now
   that you have matched all possible groups? One possible solution, delete the
   matched node, balance the tree and then again start searching for matches
   from the "new" root. */

#define MATCHLEN 6 		/* Default variable match length. */

struct tnode
{
  char *word;
  struct tnode *left;
  struct tnode *right;
};

struct lnode
{
  char *word;
  struct lnode *next;
}

/* Add W to tree node pointer P. */
struct tnode *
tree_add (struct tnode *p, char *w)
{
  int cond;

  if (p == NULL)
    {
      p = (struct tnode *) malloc (sizeof (struct tnode));
      p->word = strdup (w);
      p->left = p->right = NULL;
    }
  else if ((cond = strcmp (w, p->word)) < 0)
    p->left = addtree (p->left, w);
  else if (cond > 0)
    p->right = addtree (p->right, w);

  return p;
}

void
tree_print (struct tnode *p)
{
  if (p != NULL)
    {
      tree_print (p->left);
      printf ("%4d %s\n", p->count, p->word);
      tree_print (p->right);
    }
}

struct lnode *
list_add (struct lnode *p, char *w)
{
  if (p != NULL)
    p->next = list_add (p->next, w);
  else
    {
      p = (struct lnode *) malloc (sizeof (struct lnode));
      p->word = strdup (w);
      p->next = NULL;
    }
}

void
list_print (struct lnode *p)
{
  if (p != NULL)
    {
      puts (p->word);
      list_print (p->next);
    }
}

int
main (void)
{
  struct tnode *troot;
  struct lnode *lroot;
  char word[MAXWORD];
  size_t len;

  len = MATCHLEN;
  troot = NULL;
  while (getword (word, MAXWORD) != EOF)
    {
      if (len < strlen (word))
	root = tree_add (root, word);
    }
  return 0;
}
