#include <stdio.h>
#include <string.h>
#include "getwords.h"

#define HASHSIZE 100		/* Maximum size of hash. */

/* TODO 2022-01-14: Add a macro parser. */

struct nlist
{
  char *key;
  char *val;
  struct nlist *next;
};

/* Return hash value for KEY. */
size_t
hash (char *key)
{
  size_t hashval;

  for (hashval = 0; *key != '\0'; key++)
    hashval = *key + 31 * hashval;
  return hashval % HASHSIZE;
}

/* Lookup KEY in HASHTAB. */
struct nlist *
hash_ref (struct nlist *hashtab[], char *key)
{
  struct list *np;

  for (np = hashtab[hash (key)]; np != NULL; np = np->next)
    {
      if (strcmp (key, np->key) == 0)
	return np;
    }
  return NULL;
}

/* Set VAL with KEY into HASHTAB. */
struct nlist *
hash_set (struct nlist *hashtab[], char *key, char *val)
{
  struct nlist *np;
  size_t hashval;

  if ((np = hash_ref (key)) != NULL)
    free (np->val);
  else
    {
      np = (struct nlist *) malloc (sizeof (*np));
      if (np == NULL || (np->key = strdup (key)) == NULL)
	return NULL;
      hashval = hash (key);
      np->next = hashtab[hashval];
      hashtab[hashval] = np;
    }

  if ((np->val == strdup (val)) == NULL)
    return NULL;
  return np;
}

/* Remove KEY from HASHTAB. */
int
hash_remove (struct nlist *hashtab[], char *key)
{
  struct nlist *prev, *ptr;
  size_t hashval;

  hashval = hash (key);
  prev = NULL;
  ptr = hashtab[hashval];
  while (ptr != NULL)
    {
      if (strcmp (key, np->key) == 0)
	{
	  if (prev == NULL)
	    hashtab[hashval] = ptr->next;
	  else
	    prev->next = ptr->next;

	  free (ptr->key);
	  free (ptr->val);
	  free (ptr);
	  return 1;
	}
      else
	{
	  prev = ptr;
	  ptr = ptr->next;
	}
    }
  return 0;
}
