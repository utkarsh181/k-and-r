#include <stdio.h>
#include <ctype.h>

/* Get WORD with maximum limit LIM from STREAM.  A word is defined as:
   - A string of letter and digits beginning with a letter.
   - A comment.
   - A string constant
   - A preprocessor control line.
   - Else a single non-white space character. */
int
getword (char *word, int lim, FILE *stream)
{
  int c;
  char *w = word;

  while (isspace (c = getc (stream)))
    ;

  if (c != EOF)
    *w++ = c;

  if (c == '"')			/* String constants. */
    {
      for ( ; --lim > 0; w++)
	{
	  if ((*w = getc (stream)) == '"')
	    {
	      w++;
	      break;
	    }
	}
    }
  else if (c == '/')		/* Comments */
    {
      *w = getc (stream);
      if (*w != '*')
	{
	  ungetc (*w, stream);
	  *w = '\0';
	  return c;
	}
      else
	{
	  w++;			/* Store '*'. */
	  for ( ; --lim > 0; w++)
	    {
	      if ((*w = getc (stream)) == '*')
		{
		  w++;
		  if ((*w = getc (stream)) == '/')
		    {
		      w++;
		      break;
		    }
		}
	    }
	}
    }
  /* TODO 2022-01-14: A C pre-processor statement can be a multi-line statement,
     checkout the complete specification in K&R2 Section A12 Pg. 228. */
  else if (c == '#')		/* A preprocessor control line. */
    {
      for ( ; --lim > 0; w++)
	{
	  if ((*w = getc (stream)) == '\n')
	    {
	      ungetc (*w, stream);
	      break;
	    }
	}
    }
  else if (isalpha (c))
    {
      for ( ; --lim > 0; w++)
	{
	  if (!isalnum (*w = getc (stream)))
	    {
	      ungetc (*w, stream);
	      break;
	    }
	}
    }
  else
    {
      *w = '\0';
      return c;
    }

  *w = '\0';
  return word[0];
}

/* Is S a C comment. */
int
iscomment (char *s)
{
  size_t len = strlen (s);

  return len > 4
    && s[0] == '/'
    && s[1] == '*'
    && s[len - 1] == '/'
    && s[len - 2] == '*';
}

/* Is S a string constant. */
int
isstringconstant (char *s)
{
  size_t len = strlen (s);

  return len > 1
    && s[0] == '"'
    && s[len - 1] == '"';
}
