/* ex-6.3.c --- A cross-refrencer */

/* This program is a cross-refrencer that prints a list of all words in a
   document, and, for each, a list of line numbers on which it occurs. */

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/* TODO 2022-01-12: Add noice cancellation filter. */

#define MAXWORD 1000		/* Maximum length of word. */

/* The list node. */
struct lnode
{
  size_t line_number;		/* The line number. */
  struct lnode *next;		/* Next node. */
};

/* The tree node. */
struct tnode
{
  char *word;			/* Pointer to the text. */
  struct lnode *lno;		/* Pointer to list of line numbers. */
  struct tnode *left;		/* Left child. */
  struct tnode *right;		/* Right child. */
};

/* Add LINE_NUMBER to list node pointer P. */
struct lnode *
list_add (struct lnode *p, size_t line_number)
{
  if (p != NULL)
    p->next = list_add (p->next, line_number);
  else
    {
      p = (struct lnode *) malloc (sizeof (struct lnode));
      p->line_number = line_number;
      p->next =NULL;
    }
  return p;
}

/* Case intensively compare S and T. */
int
istrcmp (const char *s, const char *t)
{
  int diff = 0;
  char cs = 0;
  char ct = 0;

  while (diff == 0 && *s != '\0' && *t != '\0')
    {
      cs = tolower ((unsigned char) *s);
      ct = tolower ((unsigned char) *t);
      if (cs < ct)
	{
	  diff = -1;
	}
      else if (cs > ct)
	{
	  diff = 1;
	}
      ++s;
      ++t;
    }

  if (diff == 0 && *s != *t)
    {
      /* the shorter string comes lexicographically sooner */
      if (*s == '\0')
	{
	  diff = -1;
	}
      else
	{
	  diff = 1;
	}
    }

  return diff;
}

/* Add W to tree node pointer P. */
struct tnode *
tree_add (struct tnode *p, char *w, size_t line_number)
{
  int cond;

  if (p == NULL)
    {
      p = (struct tnode *) malloc (sizeof (struct tnode));
      p->word = strdup (w);
      p->lno = NULL;
      p->lno = list_add (p->lno, line_number);
      p->left = p->right = NULL;
    }
  else if ((cond = istrcmp (w, p->word)) == 0)
    p->lno = list_add (p->lno, line_number);
  else if (cond < 0)
    p->left = tree_add (p->left, w, line_number);
  else if (cond > 0)
    p->right = tree_add (p->right, w, line_number);

  return p;
}

void
list_print (struct lnode *p)
{
  if (p != NULL)
    {
      if (p->next == NULL)
	printf ("%6d\n", p->line_number);
      else
	printf ("%6d ", p->line_number);
      list_print (p->next);
    }
}

void
tree_print (struct tnode *p)
{
  if (p != NULL)
    {
      tree_print (p->left);
      printf ("%18s ", p->word);
      list_print (p->lno);
      tree_print (p->right);
    }
}

/* Is C a delimiter? */
int
isdelim (char c)
{
  char *delims = " \t\n\r\a\f\v!\"%^&*()_=+{}[]\\|/,.<>:;#~?";
  return strchr (delims, c) != NULL;
}

char *
getword (char *line, char *word, int lim)
{
  if (line == NULL || *line == '\0')
    return NULL;
  else
    {
      char c;
      char *w = word;

      while (isspace (c = *line++))
	;
      if (c != '\0')
	*w++ = c;
      for ( ; --lim > 0; w++, line++)
	{
	  if (isdelim (*w = *line))
	    break;
	}
      *w = '\0';
      return line;
    }
}

int
main (void)
{
  char *line;
  char word[MAXWORD];
  FILE *stream;
  size_t len, nlines;
  ssize_t nread;
  struct tnode *root;

  root = NULL;
  len = nlines = 0;
  stream = stdin;
  while ((nread = getline (&line, &len, stream)) != -1)
    {
      while ((line = getword (line, word, MAXWORD)) != NULL)
	  root = tree_add (root, word, nlines);
      nlines++;
    }
  printf ("%18s Line numbers\n", "Words");
  tree_print (root);
  return 0;
}
